# Advent of Code

This repo houses my solutions to the [Advent of Code](https://adventofcode.com/) programming advent calender, from 2023 onwards.

My goal is to have each year complete in under 1s (on my macbook), while not taking shortcuts with the solutions.

Previous years' solutions will eventually find their way in here, while I am continuing to learn Rust.
