#![warn(
    clippy::all,
    // clippy::restriction,
    clippy::pedantic,
    clippy::nursery,
    // clippy::cargo,
    clippy::unseparated_literal_suffix,
)]

#[macro_use]
mod utils;

macro_rules! run_all_results {
    ($year:tt,  $($day:tt),+ ,) => {
        mod $year {$(pub mod $day;)+
        pub fn run() {
                println!("");
                println!(" {:4} | {:^16} | {:^40} | {:9} ", stringify!($year).get(3..).unwrap(), "Part One", "Part Two", "Time [ms]");
                println!("{:-<80}", "");
            $(
                let a = std::time::Instant::now();
                let t = $day::run();
                println!(" {:^4} | {:>16} | {:>40} | {:>9} ", stringify!($day).get(3..).unwrap(), t.0, t.1, a.elapsed().as_millis());
            )+
        }}
    }
}

run_all_results!(
    aoc2023, day01, day02, day03, day04, day05, day06, day07, day08, day09, day10, day11, day12,
    day13, day14, day15, day16, day17, day18, day19, day20, day21, day22, day23, day24, day25,
);
run_all_results!(
    aoc2024, day01, day02, day03, day04, day05, day06, day07, day08, day09, day10, day11, day12,
    day13, day14, day15, day16, // day17,
    day18, day19, day20, day21, day22, day23, day24, day25,
);

fn run_year(f: impl Fn()) {
    let a = std::time::Instant::now();
    f();
    println!("Total time: {:?}ms", a.elapsed().as_millis());
}

fn main() {
    run_year(aoc2023::run);
    run_year(aoc2024::run);
}
