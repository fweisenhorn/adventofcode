const INPUT: &str = include_str!("inputs/day04.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    input
        .lines()
        .map(parse_line)
        .filter(|[a, b, c, d]| (a <= c && b >= d) || (a >= c && b <= d))
        .count()
        .to_string()
}

fn part_b(input: &str) -> String {
    input
        .lines()
        .map(parse_line)
        .filter(|[a, b, c, d]| a <= d && b >= c)
        .count()
        .to_string()
}

fn parse_line(line: &str) -> [u32; 4] {
    let (first, second) = line.split_once(',').expect("Input should parse.");
    let (a, b) = first.split_once('-').expect("Input should parse.");
    let (c, d) = second.split_once('-').expect("Input should parse.");

    [
        a.parse().expect("Input should parse."),
        b.parse().expect("Input should parse."),
        c.parse().expect("Input should parse."),
        d.parse().expect("Input should parse."),
    ]
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day04.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "2");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "441");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "4");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "861");
    }
}
