const INPUT: &str = include_str!("inputs/day20.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    todo!()
}

fn part_b(input: &str) -> String {
    todo!()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day20.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "");
    }

    #[test]
    #[ignore]
    fn test_b() {
        assert_eq!(part_b(TEST), "");
    }

    #[test]
    #[ignore]
    fn result_a() {
        assert_eq!(part_a(INPUT), "");
    }

    #[test]
    #[ignore]
    fn result_b() {
        assert_eq!(part_b(INPUT), "");
    }
}
