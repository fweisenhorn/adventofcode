const INPUT: &str = include_str!("inputs/day02.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    input
        .lines()
        .map(|line| match line {
            "A X" => 4,
            "A Y" => 8,
            "A Z" => 3,
            "B X" => 1,
            "B Y" => 5,
            "B Z" => 9,
            "C X" => 7,
            "C Y" => 2,
            "C Z" => 6,
            _ => unreachable!(),
        })
        .sum::<u32>()
        .to_string()
}

fn part_b(input: &str) -> String {
    input
        .lines()
        .map(|line| match line {
            "A X" => 3,
            "A Y" => 4,
            "A Z" => 8,
            "B X" => 1,
            "B Y" => 5,
            "B Z" => 9,
            "C X" => 2,
            "C Y" => 6,
            "C Z" => 7,
            _ => unreachable!(),
        })
        .sum::<u32>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day02.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "15");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "9759");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "12");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "12429");
    }
}
