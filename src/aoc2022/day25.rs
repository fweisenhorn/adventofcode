const INPUT: &str = include_str!("inputs/day25.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    todo!()
}

fn part_b(_: &str) -> String {
    '🎄'.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day25.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "🎄");
    }

    #[test]
    #[ignore]
    fn result_a() {
        assert_eq!(part_a(INPUT), "");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "🎄");
    }
}
