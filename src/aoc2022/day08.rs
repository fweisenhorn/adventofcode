const INPUT: &str = include_str!("inputs/day08.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    let data: Vec<&[u8]> = input.lines().map(str::as_bytes).collect();
    let mut counter = 0;

    for y in 0..data.len() {
        for x in 0..data[0].len() {
            if (0..y).all(|i| data[i][x] < data[y][x])
                || (y + 1..data.len()).all(|i| data[y][x] > data[i][x])
                || (0..x).all(|i| data[y][i] < data[y][x])
                || (x + 1..data[0].len()).all(|i| data[y][x] > data[y][i])
            {
                counter += 1;
            }
        }
    }

    counter.to_string()
}

fn part_b(input: &str) -> String {
    let data: Vec<&[u8]> = input.lines().map(str::as_bytes).collect();

    let mut out = 0;

    for y in 1..(data.len() - 1) {
        for x in 1..(data[0].len() - 1) {
            let mut counter = 1;
            let mut i = 1;
            while y + i + 1 < data.len() && data[y + i][x] < data[y][x] {
                i += 1;
            }
            counter *= i;

            i = 1;
            while i < y && data[y - i][x] < data[y][x] {
                i += 1;
            }
            counter *= i;

            i = 1;
            while x + i + 1 < data[0].len() && data[y][x + i] < data[y][x] {
                i += 1;
            }
            counter *= i;

            i = 1;
            while i < x && data[y][x - i] < data[y][x] {
                i += 1;
            }
            counter *= i;

            if counter > out {
                out = counter;
            }
        }
    }

    out.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day08.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "21");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "1803");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "8");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "268912");
    }
}
