use std::collections::HashSet;

use crate::utils::direction::Direction;
use crate::utils::position::{Position, SignedPos};

const INPUT: &str = include_str!("inputs/day09.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    let mut head = SignedPos::default();
    let mut tail = SignedPos::default();
    let mut visited: HashSet<SignedPos> = HashSet::new();
    visited.insert(tail);

    input.lines().for_each(|line| {
        let (a, b) = line.split_once(' ').expect("Input should parse.");
        let dir = Direction::try_from(a.chars().next().unwrap()).expect("Input should parse.");
        let b = b.parse::<u16>().expect("Input should parse.");

        for _ in 0..b {
            head = head.steps_unchecked(dir, 1);

            if (tail.distance(&head) == 2 && (tail.0 == head.0 || tail.1 == head.1))
                || tail.distance(&head) > 2
            {
                tail = Position::new(
                    tail.0 + (head.0 - tail.0).signum(),
                    tail.1 + (head.1 - tail.1).signum(),
                );
                visited.insert(tail);
            }
        }
    });

    visited.len().to_string()
}

fn part_b(input: &str) -> String {
    let mut snake = [SignedPos::default(); 10];
    let mut visited = HashSet::<SignedPos>::new();

    input.lines().for_each(|line| {
        let (a, b) = line.split_once(' ').expect("Input should parse.");
        let dir = Direction::try_from(a.chars().next().unwrap()).expect("Input should parse.");
        let b = b.parse::<u16>().expect("Input should parse.");

        for _ in 0..b {
            snake[0] = snake[0].steps_unchecked(dir, 1);

            for i in 1..10 {
                if (snake[i].distance(&snake[i - 1]) == 2
                    && (snake[i].0 == snake[i - 1].0 || snake[i].1 == snake[i - 1].1))
                    || snake[i].distance(&snake[i - 1]) > 2
                {
                    snake[i] = Position(
                        snake[i].0 + (snake[i - 1].0 - snake[i].0).signum(),
                        snake[i].1 + (snake[i - 1].1 - snake[i].1).signum(),
                    );
                }
            }

            visited.insert(snake[9]);
        }
    });

    visited.len().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day09.txt");
    const TEST_: &str = include_str!("tests/day09a.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "13");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "6243");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST_), "36");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "2630");
    }
}
