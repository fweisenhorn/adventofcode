use crate::utils::{direction::Direction, position::Position};
use std::{cmp, collections::HashSet};

const INPUT: &str = include_str!("inputs/day14.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    let walls: HashSet<Position<u32>> = parse_input(input);
    let mut sands: HashSet<Position<u32>> = HashSet::new();

    let max_height = walls
        .iter()
        .map(|p| p.0)
        .max()
        .expect("Should have a maximum.");

    'outer: loop {
        let mut currently_falling = Position::<u32>(0, 500);
        'inner: loop {
            if currently_falling.0 > max_height {
                break 'outer;
            }

            let down_one = currently_falling.steps_unchecked(Direction::Down, 1);
            let down_left = down_one.steps_unchecked(Direction::Left, 1);
            let down_right = down_one.steps_unchecked(Direction::Right, 1);

            if !(sands.contains(&down_one) || walls.contains(&down_one)) {
                currently_falling = down_one;
                continue;
            } else if !(sands.contains(&down_left) || walls.contains(&down_left)) {
                currently_falling = down_left;
                continue;
            } else if !(sands.contains(&down_right) || walls.contains(&down_right)) {
                currently_falling = down_right;
                continue;
            }
            sands.insert(currently_falling);
            break 'inner;
        }
    }

    sands.len().to_string()
}

fn part_b(input: &str) -> String {
    let walls = parse_input(input);
    let mut sands = Vec::new();

    let max_height = walls
        .iter()
        .map(|p| p.0)
        .max()
        .expect("Should have a maximum.");

    let mut curr_row: HashSet<Position<u32>> = HashSet::new();
    let mut next_row: HashSet<Position<u32>> = HashSet::new();

    curr_row.insert(Position::<u32>(0, 500));

    'outer: loop {
        for pos in &curr_row {
            println!("{pos:?}");
            let down_one = pos.steps_unchecked(Direction::Down, 1);
            let down_left = down_one.steps_unchecked(Direction::Left, 1);
            let down_right = down_one.steps_unchecked(Direction::Right, 1);
            if down_one.0 > max_height + 2 {
                break 'outer;
            }

            if !walls.contains(&down_one) {
                next_row.insert(down_one);
            }
            if !walls.contains(&down_left) {
                next_row.insert(down_left);
            }
            if !walls.contains(&down_right) {
                next_row.insert(down_right);
            }
        }
        sands.extend(curr_row.drain());
        curr_row.extend(next_row.drain());
    }

    sands.len().to_string()
}

fn parse_input(input: &str) -> HashSet<Position<u32>> {
    input
        .lines()
        .flat_map(|line| {
            let t: Vec<&str> = line.split(" -> ").collect();

            t.windows(2)
                .flat_map(|x| convert_to_points(x[0], x[1]).expect("Input should parse."))
                .collect::<Vec<Position<u32>>>()
        })
        .collect()
}

fn convert_to_points(a: &str, b: &str) -> Option<Vec<Position<u32>>> {
    let Position(x1, y1) = to_point(a)?;
    let Position(x2, y2) = to_point(b)?;

    if x1 == x2 && y1 == y2 {
        Some(vec![Position::<u32>(x1, y1)])
    } else if x1 == x2 {
        let t: Vec<Position<u32>> = (cmp::min(y1, y2)..=cmp::max(y1, y2))
            .map(|y| Position::<u32>(x1, y))
            .collect();
        Some(t)
    } else if y1 == y2 {
        let t: Vec<Position<u32>> = (cmp::min(x1, x2)..=cmp::max(x1, x2))
            .map(|x| Position::<u32>(x, y1))
            .collect();
        Some(t)
    } else {
        None
    }
}

fn to_point(p: &str) -> Option<Position<u32>> {
    let (x, y) = p.split_once(',')?;

    Some(Position::<u32>(
        y.parse::<u32>().ok()?,
        x.parse::<u32>().ok()?,
    ))
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day14.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "24");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "825");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "93");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "26729");
    }
}
