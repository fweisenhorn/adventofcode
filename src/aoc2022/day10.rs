const INPUT: &str = include_str!("inputs/day10.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    let mut register = 1;
    let mut cycles = 0;

    let mut cycle_read = 20;
    let mut out = 0;

    for line in input.lines() {
        if cycles + 2 >= cycle_read {
            out += cycle_read * register;
            cycle_read += 40;
            if cycle_read > 240 {
                break;
            }
        }

        if line == "noop" {
            cycles += 1;
        } else if let Some((_, b)) = line.split_once(' ') {
            register += b.trim().parse::<i32>().expect("Input should parse.");
            cycles += 2;
        } else {
            unreachable!()
        }
    }

    out.to_string()
}

fn part_b(input: &str) -> String {
    let mut register = 1;
    let mut cycle = 0;
    let mut out = String::with_capacity(240);

    for line in input.lines() {
        if line == "noop" {
            append_symbol(&mut out, &mut cycle, register);
        } else if let Some((_, b)) = line.split_once(' ') {
            append_symbol(&mut out, &mut cycle, register);
            append_symbol(&mut out, &mut cycle, register);
            register += b.trim().parse::<i32>().expect("Input should parse.");
        } else {
            unreachable!()
        }

        if cycle >= 240 {
            break;
        }
    }

    out
}

fn append_symbol(s: &mut String, cycle: &mut i32, register: i32) {
    if (*cycle % 40) >= register - 1 && (*cycle % 40) <= register + 1 {
        *s += "#";
    } else {
        *s += ".";
    }

    if *cycle % 40 == 39 && *cycle != 239 {
        *s += "\n";
    }

    *cycle += 1;
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day10.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "13140");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "13720");
    }

    #[test]
    fn test_b() {
        assert_eq!(
            part_b(TEST),
            "\
            ##..##..##..##..##..##..##..##..##..##..\n\
            ###...###...###...###...###...###...###.\n\
            ####....####....####....####....####....\n\
            #####.....#####.....#####.....#####.....\n\
            ######......######......######......####\n\
            #######.......#######.......#######....."
        );
    }

    #[test]
    fn result_b() {
        assert_eq!(
            part_b(INPUT),
            "\
            ####.###..#..#.###..#..#.####..##..#..#.\n\
            #....#..#.#..#.#..#.#..#....#.#..#.#..#.\n\
            ###..###..#..#.#..#.####...#..#....####.\n\
            #....#..#.#..#.###..#..#..#...#....#..#.\n\
            #....#..#.#..#.#.#..#..#.#....#..#.#..#.\n\
            #....###...##..#..#.#..#.####..##..#..#."
        );
    }
}
