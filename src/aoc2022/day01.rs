const INPUT: &str = include_str!("inputs/day01.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    parse_input(input).iter().max().unwrap().to_string()
}

fn part_b(input: &str) -> String {
    let mut elves = parse_input(input);
    elves.sort_unstable();
    elves.reverse();
    assert!(elves.len() >= 3);
    elves[0..3].iter().sum::<usize>().to_string()
}

fn parse_input(input: &str) -> Vec<usize> {
    let mut elves = vec![0];
    for line in input.lines() {
        if line.is_empty() {
            elves.push(0);
        } else {
            *elves.last_mut().unwrap() += line.parse::<usize>().expect("Input should parse");
        }
    }
    elves
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day01.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "24000");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "69289");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "45000");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "205615");
    }
}
