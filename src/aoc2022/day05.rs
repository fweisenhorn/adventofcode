const INPUT: &str = include_str!("inputs/day05.txt");

type Stack = Vec<char>;

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    let (mut stacks, instrns) = parse_input(input);

    for instr in instrns {
        assert!(instr[1] <= stacks.len());
        assert!(instr[2] <= stacks.len());

        let from = stacks[instr[1] - 1].clone();

        assert!(instr[0] <= from.len());

        stacks[instr[2] - 1].extend(from.iter().rev().take(instr[0]));

        for _ in 0..instr[0] {
            stacks[instr[1] - 1].pop();
        }
    }

    read_stack_tops(&stacks)
}

fn part_b(input: &str) -> String {
    let (mut stacks, instrns) = parse_input(input);

    for instr in instrns {
        assert!(instr[1] <= stacks.len());
        assert!(instr[2] <= stacks.len());

        let from = stacks[instr[1] - 1].clone();

        assert!(instr[0] <= from.len());

        stacks[instr[2] - 1].extend(from.iter().rev().take(instr[0]).rev());

        for _ in 0..instr[0] {
            stacks[instr[1] - 1].pop();
        }
    }

    read_stack_tops(&stacks)
}

fn parse_input(input: &str) -> (Vec<Stack>, Vec<[usize; 3]>) {
    let (a, b) = input.split_once("\n\n").expect("Input should parse");

    let n_stacks = a.lines().last().unwrap().len() / 4;
    let mut out = vec![Vec::with_capacity(a.lines().count()); n_stacks + 1];

    for line in a.lines() {
        for (i, c) in line.chars().enumerate() {
            if c.is_alphabetic() {
                assert_eq!((i - 1) % 4, 0);
                out[(i - 1) / 4].insert(0, c);
            }
        }
    }

    let instrns = b.lines().map(parse_instruction).collect();

    (out, instrns)
}

fn parse_instruction(input: &str) -> [usize; 3] {
    let t: Vec<&str> = input.split_whitespace().collect();
    assert_eq!(t.len(), 6);

    [
        t[1].parse().unwrap(),
        t[3].parse().unwrap(),
        t[5].parse().unwrap(),
    ]
}

fn read_stack_tops(stacks: &[Stack]) -> String {
    let mut out = String::new();
    for stack in stacks {
        out.push(*stack.last().expect("Stack should not be empty"));
    }
    out
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day05.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "CMZ");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "FWNSHLDNZ");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "MCD");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "RNRGDNFQG");
    }
}
