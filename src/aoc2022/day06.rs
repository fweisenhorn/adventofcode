use std::collections::HashSet;

const INPUT: &str = include_str!("inputs/day06.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    find_first_uniques(input, 4)
}

fn part_b(input: &str) -> String {
    find_first_uniques(input, 14)
}

fn find_first_uniques(input: &str, n: usize) -> String {
    let t: Vec<char> = input.chars().collect();
    for (i, window) in t.windows(n).enumerate() {
        if HashSet::<char>::from_iter(window.to_owned()).len() == n {
            return (i + n).to_string();
        }
    }

    unreachable!()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_1: &str = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";
    const TEST_2: &str = "bvwbjplbgvbhsrlpgdmjqwftvncz";
    const TEST_3: &str = "nppdvjthqldpwncqszvftbrmjlhg";
    const TEST_4: &str = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg";
    const TEST_5: &str = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST_1), "7");
        assert_eq!(part_a(TEST_2), "5");
        assert_eq!(part_a(TEST_3), "6");
        assert_eq!(part_a(TEST_4), "10");
        assert_eq!(part_a(TEST_5), "11");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "1855");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST_1), "19");
        assert_eq!(part_b(TEST_2), "23");
        assert_eq!(part_b(TEST_3), "23");
        assert_eq!(part_b(TEST_4), "29");
        assert_eq!(part_b(TEST_5), "26");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "3256");
    }
}
