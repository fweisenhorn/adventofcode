const INPUT: &str = include_str!("inputs/day11.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    let (monkez, mut held_items): (Vec<Monkey>, Vec<Vec<usize>>) = parse_input(input);
    let mut counter: Vec<usize> = vec![0; monkez.len()];

    for _ in 0..20 {
        for (i, active_monkey) in monkez.iter().enumerate() {
            counter[i] += held_items[i].len();

            let it: Vec<usize> = held_items[i].drain(..).collect();
            for item in &it {
                let item_new = active_monkey.op.apply(*item) / 3;
                if item_new % active_monkey.test_divides_int == 0 {
                    held_items[active_monkey.test_pass_monkey_id].push(item_new);
                } else {
                    held_items[active_monkey.test_fail_monkey_id].push(item_new);
                }
            }
        }
    }

    counter.sort_unstable();
    counter.reverse();
    counter[0..2].iter().product::<usize>().to_string()
}

fn part_b(input: &str) -> String {
    let (monkez, mut held_items): (Vec<Monkey>, Vec<Vec<usize>>) = parse_input(input);
    let mut counter: Vec<usize> = vec![0; monkez.len()];

    let lcm: usize = monkez.iter().map(|m| m.test_divides_int).product::<usize>();

    for _ in 0..10_000 {
        for (i, active_monkey) in monkez.iter().enumerate() {
            counter[i] += held_items[i].len();

            let it: Vec<usize> = held_items[i].drain(..).collect();
            for item in &it {
                let item_new = active_monkey.op.apply(*item);
                if item_new % active_monkey.test_divides_int == 0 {
                    held_items[active_monkey.test_pass_monkey_id].push(item_new % lcm);
                } else {
                    held_items[active_monkey.test_fail_monkey_id].push(item_new % lcm);
                }
            }
        }
    }

    counter.sort_unstable();
    counter.reverse();
    counter[0..2].iter().product::<usize>().to_string()
}

fn parse_input(input: &str) -> (Vec<Monkey>, Vec<Vec<usize>>) {
    let mut out_2: Vec<Vec<usize>> = Vec::new();
    let out_1: Vec<Monkey> = input
        .split("\n\n")
        .map(|string: &str| {
            let mut it = string.lines();
            it.next();
            let t = it.next().expect("Starting items line should exist.");
            let (_, t) = t
                .split_once(": ")
                .expect("Starting items line should parse.");
            let t = t
                .split(", ")
                .map(|s| s.parse::<usize>().expect("Starting items should parse."))
                .collect();
            out_2.push(t);

            let t = it.next().expect("Operation line should exist.");
            let (_, t) = t
                .split_once("old ")
                .expect("Operation items line should parse.");
            let x = t
                .split_once(' ')
                .expect("Operation items line should parse.");
            let monk_op = match x {
                ("*", "old") => Operation::Square,
                ("*", s) => Operation::Mult(s.parse::<usize>().expect("Operation should parse.")),
                ("+", s) => Operation::Add(s.parse::<usize>().expect("Operation should parse.")),
                _ => unreachable!(),
            };

            let t = it.next().expect("Test line should exist.");
            let (_, t) = t.split_once("by ").expect("Test line should parse.");
            let monk_test = t.parse::<usize>().expect("Test should parse.");

            let t = it.next().expect("Test line should exist.");
            let (_, t) = t.split_once("monkey ").expect("Test line should parse.");
            let monk_pos = t.parse::<usize>().expect("Test should parse.");

            let t = it.next().expect("Test line should exist.");
            let (_, t) = t.split_once("monkey ").expect("Test line should parse.");
            let monk_neg = t.parse::<usize>().expect("Test should parse.");

            Monkey {
                op: monk_op,
                test_divides_int: monk_test,
                test_pass_monkey_id: monk_pos,
                test_fail_monkey_id: monk_neg,
            }
        })
        .collect();

    (out_1, out_2)
}

enum Operation {
    Add(usize),
    Mult(usize),
    Square,
}

impl Operation {
    fn apply(&self, item: usize) -> usize {
        match self {
            Self::Add(i) => item + i,
            Self::Mult(i) => item * i,
            Self::Square => item * item,
        }
    }
}

struct Monkey {
    op: Operation,
    test_divides_int: usize,
    test_pass_monkey_id: usize,
    test_fail_monkey_id: usize,
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day11.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "10605");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "112221");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "2713310158");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "25272176808");
    }
}
