use std::collections::{BinaryHeap, HashMap};

use crate::utils::{direction::NEIGHBOURS, position::Position};

const INPUT: &str = include_str!("inputs/day12.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    let (heightmap, start_pos, end_pos) = parse_input(input);
    dijkstra(&heightmap, &start_pos, &end_pos)
        .expect("Should find path.")
        .to_string()
}

fn part_b(input: &str) -> String {
    let (height_map, _, end_pos) = parse_input(input);

    let mut start_poss: Vec<Position<usize>> =
        Vec::with_capacity(height_map.len() * height_map.len() / 26);
    for (i, row) in height_map.iter().enumerate() {
        for (j, c) in row.iter().enumerate() {
            if *c == b'a' {
                start_poss.push(Position(i, j));
            }
        }
    }

    start_poss
        .iter()
        .filter_map(|start_pos| dijkstra(&height_map, start_pos, &end_pos))
        .min()
        .expect("Some min should be found.")
        .to_string()
}

fn parse_input(input: &str) -> (Vec<Vec<u8>>, Position<usize>, Position<usize>) {
    let mut heightmap: Vec<Vec<u8>> = input
        .lines()
        .map(|line| line.as_bytes().to_owned())
        .collect();
    let start_pos = {
        let x = heightmap
            .iter()
            .position(|v| v.contains(&b'S'))
            .expect("Input should parse.");
        Position(
            x,
            heightmap[x]
                .iter()
                .position(|v| v == &b'S')
                .expect("Input should parse."),
        )
    };
    let end_pos = {
        let x = heightmap
            .iter()
            .position(|v| v.contains(&b'E'))
            .expect("Input should parse.");
        Position(
            x,
            heightmap[x]
                .iter()
                .position(|v| v == &b'E')
                .expect("Input should parse."),
        )
    };

    heightmap[start_pos.0][start_pos.1] = b'a';
    heightmap[end_pos.0][end_pos.1] = b'z';

    (heightmap, start_pos, end_pos)
}

fn dijkstra(
    height_map: &[Vec<u8>],
    start_pos: &Position<usize>,
    end_pos: &Position<usize>,
) -> Option<usize> {
    let mut cache: HashMap<Position<usize>, usize> =
        HashMap::with_capacity(height_map.len() * height_map.len());
    let mut queue: BinaryHeap<HeapKey> =
        BinaryHeap::<HeapKey>::with_capacity(height_map.len() * height_map.len());

    queue.push(HeapKey {
        cost: 0,
        pos: *start_pos,
    });

    while let Some(HeapKey { cost: n, pos: p }) = queue.pop() {
        if p == *end_pos {
            return Some(n);
        }

        if cache.get(&p).is_some_and(|v| *v <= n) {
            continue;
        }
        cache.insert(p, n);

        NEIGHBOURS
            .iter()
            .filter_map(|d| p.try_steps(*d, 1))
            .filter(|new_pos| {
                height_map
                    .get(new_pos.0)
                    .and_then(|v| v.get(new_pos.1))
                    .is_some_and(|neigh_height| *neigh_height <= (1 + height_map[p.0][p.1]))
            })
            .for_each(|new_pos| {
                if !cache.get(&new_pos).is_some_and(|x| *x <= n) {
                    queue.push(HeapKey {
                        cost: n + 1,
                        pos: new_pos,
                    });
                }
            });
    }

    None
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
struct HeapKey {
    cost: usize,
    pos: Position<usize>,
}

impl Ord for HeapKey {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.pos.cmp(&other.pos))
    }
}

impl PartialOrd for HeapKey {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day12.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "31");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "383");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "29");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "377");
    }
}
