use std::cmp::Ordering;

const INPUT: &str = include_str!("inputs/day13.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    input
        .split("\n\n")
        .enumerate()
        .filter(|(_, pair)| {
            let (a, b) = {
                let t = pair.split_once('\n').expect("Should have two lines");
                (
                    Signal::try_from(t.0.trim()).expect("Input should parse."),
                    Signal::try_from(t.1.trim()).expect("Input should parse."),
                )
            };
            a < b
        })
        .map(|(n, _)| n + 1)
        .sum::<usize>()
        .to_string()
}

fn part_b(input: &str) -> String {
    let mut signals: Vec<Signal> = input
        .lines()
        .filter_map(|line| {
            if line.is_empty() {
                None
            } else {
                Signal::try_from(line).ok()
            }
        })
        .collect();

    let s1 = Signal::try_from("[[2]]").unwrap();
    let s2 = Signal::try_from("[[6]]").unwrap();

    signals.push(s1.clone());
    signals.push(s2.clone());

    signals.sort_unstable_by(|a, b| a.partial_cmp(b).expect("Should be sortable."));

    ((signals.iter().position(|x| *x == s1).unwrap() + 1)
        * (signals.iter().position(|x| *x == s2).unwrap() + 1))
        .to_string()
}

#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug)]
enum Possibles {
    Value(u32),
    LeftBracket,
    RightBracket,
}

#[derive(PartialEq, Eq, Hash, Clone, Default, Debug)]
struct Signal(Vec<Possibles>);

impl TryFrom<&str> for Signal {
    type Error = &'static str;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut out = Vec::with_capacity(value.len());
        let mut cur_int: Option<u32> = None;

        for cha in value.chars() {
            match cha {
                '[' if cur_int.is_none() => {
                    out.push(Possibles::LeftBracket);
                }
                ',' => {
                    if let Some(t) = cur_int.take() {
                        out.push(Possibles::Value(t));
                    }
                }
                ']' => {
                    if let Some(t) = cur_int.take() {
                        out.push(Possibles::Value(t));
                    }
                    out.push(Possibles::RightBracket);
                }
                n if n.is_numeric() => {
                    cur_int = Some(cur_int.unwrap_or(0) * 10 + n.to_digit(10).unwrap());
                }
                _ => {
                    println!();
                    println!("{value}");
                    println!("{out:?}");
                    return Err("str is not well formed.");
                }
            }
        }

        Ok(Self(out))
    }
}

impl PartialOrd for Signal {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let mut first_signal = clone_and_reverse(&self.0);
        let mut second_signal = clone_and_reverse(&other.0);

        while let (Some(c), Some(d)) = (first_signal.pop(), second_signal.pop()) {
            match (c, d) {
                (Possibles::Value(x), Possibles::Value(y)) => {
                    if x == y {
                        continue;
                    }
                    return x.partial_cmp(&y);
                }
                (Possibles::LeftBracket, Possibles::LeftBracket)
                | (Possibles::RightBracket, Possibles::RightBracket) => {
                    continue;
                }
                (Possibles::RightBracket, _) => {
                    return Some(Ordering::Less);
                }
                (_, Possibles::RightBracket) => {
                    return Some(Ordering::Greater);
                }
                (Possibles::LeftBracket, Possibles::Value(x)) => {
                    second_signal.push(Possibles::RightBracket);
                    second_signal.push(Possibles::Value(x));
                    continue;
                }
                (Possibles::Value(x), Possibles::LeftBracket) => {
                    first_signal.push(Possibles::RightBracket);
                    first_signal.push(Possibles::Value(x));
                    continue;
                }
            }
        }

        Some(Ordering::Equal)
    }
}

fn clone_and_reverse(other: &[Possibles]) -> Vec<Possibles> {
    let mut out = other.to_owned();
    out.reverse();
    out
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day13.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "13");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "5506");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "140");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "21756");
    }
}
