const INPUT: &str = include_str!("inputs/day07.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    let node = parse_input(input);
    let mut counter = 0;
    return node.traverse_part_a(&mut counter).to_string();
}

fn part_b(input: &str) -> String {
    todo!()
}

fn parse_input(input: &str) -> Dir {
    todo!()
}

#[derive(Clone)]
struct Dir {
    files: usize,
    subdirs: Vec<Dir>,
}

impl Dir {
    fn size(&self) -> usize {
        self.files + self.subdirs.iter().map(Dir::size).sum::<usize>()
    }

    fn traverse_part_a(&self, counter: &mut usize) -> usize {
        let mut out = self.files;
        for child in self.subdirs.clone() {
            out += child.traverse_part_a(counter);
        }
        if out <= 100_000 {
            *counter += out;
        }
        return out;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day07.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "95437");
    }

    #[test]
    #[ignore]
    fn result_a() {
        assert_eq!(part_a(INPUT), "1232307");
    }

    #[test]
    #[ignore]
    fn test_b() {
        assert_eq!(part_b(TEST), "24933642");
    }

    #[test]
    #[ignore]
    fn result_b() {
        assert_eq!(part_b(INPUT), "7268994");
    }
}
