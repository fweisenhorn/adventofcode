use std::collections::HashSet;

const INPUT: &str = include_str!("inputs/day03.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

fn part_a(input: &str) -> String {
    input
        .lines()
        .map(|line| -> u32 {
            let (a, b) = line.split_at(line.len() / 2);

            let mut a: HashSet<_> = a.chars().collect();
            let b: HashSet<_> = b.chars().collect();

            a.retain(|x| b.contains(x));

            assert_eq!(a.len(), 1);

            char_val(*a.iter().next().unwrap())
        })
        .sum::<u32>()
        .to_string()
}

fn part_b(input: &str) -> String {
    let lines: Vec<&str> = input.lines().collect();

    lines[..]
        .chunks(3)
        .map(|lines_triple| -> u32 {
            match lines_triple {
                [a, b, c] => {
                    let mut a: HashSet<_> = a.chars().collect();
                    let b: HashSet<_> = b.chars().collect();
                    let c: HashSet<_> = c.chars().collect();

                    a.retain(|x| b.contains(x) && c.contains(x));

                    assert_eq!(a.len(), 1);

                    char_val(*a.iter().next().unwrap())
                }
                _ => unreachable!(),
            }
        })
        .sum::<u32>()
        .to_string()
}

fn char_val(c: char) -> u32 {
    match c {
        c if c.is_ascii_lowercase() => c as u32 - 'a' as u32 + 1,
        c if c.is_ascii_uppercase() => c as u32 - 'A' as u32 + 27,
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day03.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), "157");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), "7967");
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "70");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "2716");
    }
}
