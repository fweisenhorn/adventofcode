use crate::utils::position::SignedPos;
use std::collections::HashSet;

const INPUT: &str = include_str!("inputs/day15.txt");

pub fn run() {
    println!("{}", part_a(INPUT, 2_000_000));
    println!("{}", part_b(INPUT, 4_000_000));
}

fn part_a(input: &str, row_number: isize) -> String {
    let sensors = parse_input(input);

    let mut cannot_contain: HashSet<isize> = HashSet::new();

    for (sensor, beacon) in &sensors {
        let radius = sensor.distance(beacon);

        if let Some(dist_on_line) = radius.checked_sub(sensor.1.abs_diff(row_number)) {
            for y in (sensor.0.checked_sub_unsigned(dist_on_line).unwrap())
                ..=(sensor.0.checked_add_unsigned(dist_on_line).unwrap())
            {
                cannot_contain.insert(y);
            }
        }
    }

    for (_, beacon) in &sensors {
        if beacon.1 == row_number {
            cannot_contain.remove(&beacon.0);
        }
    }

    cannot_contain.len().to_string()
}

fn part_b(input: &str, max_range: isize) -> String {
    let sensors = parse_input(input);
    let mut possibles: HashSet<SignedPos> = HashSet::new();

    for (sensor, beacon) in &sensors {
        let dist = sensor.distance(beacon);

        for pos in possibles.clone() {
            if sensor.distance(&pos) <= dist {
                possibles.remove(&pos);
            }
        }

        for dx in 0_isize.checked_sub_unsigned(dist + 1).unwrap()
            ..=0_isize.checked_add_unsigned(dist + 1).unwrap()
        {
            let x = sensor.0 + dx;

            if !inbounds(x, max_range) {
                continue;
            }

            let dy = (dist + 1) - dx.abs_diff(0);

            if let Some(y) = sensor.1.checked_sub_unsigned(dy) {
                if inbounds(y, max_range) {
                    possibles.insert(SignedPos::new(x, y));
                }
            }

            if let Some(y) = sensor.1.checked_add_unsigned(dy) {
                if inbounds(y, max_range) {
                    possibles.insert(SignedPos::new(x, y));
                }
            }
        }
    }
    for (sensor, beacon) in &sensors {
        let dist = sensor.distance(beacon);

        for pos in possibles.clone() {
            if sensor.distance(&pos) <= dist {
                possibles.remove(&pos);
            }
        }
    }

    assert_eq!(possibles.len(), 1);
    let p = possibles.iter().next().unwrap();
    (p.0 * 4_000_000 + p.1).to_string()
}

const fn inbounds(x: isize, max_range: isize) -> bool {
    0 <= x && x <= max_range
}

fn parse_input(input: &str) -> Vec<(SignedPos, SignedPos)> {
    input
        .lines()
        .map(|line| {
            let (_, line) = line.split_once("x=").unwrap();
            let (a, line) = line.split_once(", y=").unwrap();
            let (b, line) = line.split_once(": ").unwrap();
            let (_, line) = line.split_once("x=").unwrap();
            let (c, d) = line.split_once(", y=").unwrap();

            (
                SignedPos::new(a.parse().unwrap(), b.parse().unwrap()),
                SignedPos::new(c.parse().unwrap(), d.parse().unwrap()),
            )
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day15.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST, 10), "26");
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT, 2_000_000), "4502208");
    }

    #[test]
    #[ignore]
    fn test_b() {
        assert_eq!(part_b(TEST, 20), "56000011");
    }

    #[test]
    #[ignore]
    fn result_b() {
        assert_eq!(part_b(INPUT, 4_000_000), "13784551204480");
    }
}
