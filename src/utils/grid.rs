use super::position::Position;
use grid::Grid;
use std::ops::{Index, IndexMut};

impl<T, U> Index<Position<U>> for Grid<T>
where
    U: Into<usize>,
{
    type Output = T;

    fn index(&self, index: Position<U>) -> &Self::Output {
        &self[(index.0.into(), index.1.into())]
    }
}

impl<T, U> IndexMut<Position<U>> for Grid<T>
where
    U: Into<usize>,
{
    fn index_mut(&mut self, index: Position<U>) -> &mut Self::Output {
        &mut self[(index.0.into(), index.1.into())]
    }
}

//

impl<T, U> Index<&Position<U>> for Grid<T>
where
    U: Into<usize> + Copy,
{
    type Output = T;

    fn index(&self, index: &Position<U>) -> &Self::Output {
        &self[(index.0.into(), index.1.into())]
    }
}

impl<T, U> IndexMut<&Position<U>> for Grid<T>
where
    U: Into<usize> + Copy,
{
    fn index_mut(&mut self, index: &Position<U>) -> &mut Self::Output {
        &mut self[(index.0.into(), index.1.into())]
    }
}

//

pub fn find_in_grid<T: PartialEq>(needle: &T, grid: &Grid<T>) -> Option<Position<usize>> {
    grid.indexed_iter().find_map(|((x, y), t)| {
        if t == needle {
            Some(Position::new(x, y))
        } else {
            None
        }
    })
}

//

pub fn grid_from_input<T>(input: &str, f: impl Fn(char) -> T) -> Grid<T> {
    let t: Vec<Vec<T>> = input
        .lines()
        .map(|line| line.chars().map(&f).collect())
        .collect();

    Grid::from(t)
}
