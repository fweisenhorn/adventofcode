use super::direction::Direction;
use grid::Grid;
use std::hash::Hash;

#[derive(PartialOrd, Ord, PartialEq, Eq, Hash, Copy, Clone, Default, Debug)]
pub struct Position<T>(pub T, pub T);

impl<T> From<(T, T)> for Position<T> {
    fn from(value: (T, T)) -> Self {
        Self(value.0, value.1)
    }
}

impl<T: Copy + num::Integer + num::CheckedAdd + num::CheckedSub> Position<T> {
    pub const fn new(x: T, y: T) -> Self {
        Self(x, y)
    }

    pub fn steps_unchecked(&self, dir: Direction, steps: T) -> Self {
        match dir {
            Direction::Up => Self(self.0 - steps, self.1),
            Direction::Down => Self(self.0 + steps, self.1),
            Direction::Left => Self(self.0, self.1 - steps),
            Direction::Right => Self(self.0, self.1 + steps),
        }
    }

    pub fn try_steps(&self, dir: Direction, steps: T) -> Option<Self> {
        match dir {
            Direction::Up => Some(Self(self.0.checked_sub(&steps)?, self.1)),
            Direction::Down => Some(Self(self.0.checked_add(&steps)?, self.1)),
            Direction::Left => Some(Self(self.0, self.1.checked_sub(&steps)?)),
            Direction::Right => Some(Self(self.0, self.1.checked_add(&steps)?)),
        }
    }

    pub fn try_steps_with_upper_bounds(
        &self,
        dir: Direction,
        steps: T,
        x_max: T,
        y_max: T,
    ) -> Option<Self> {
        self.try_steps(dir, steps)
            .filter(|&Self(x, y)| x <= x_max && y <= y_max)
    }

    pub fn try_steps_with_bounds(
        &self,
        dir: Direction,
        steps: T,
        x_min: T,
        x_max: T,
        y_min: T,
        y_max: T,
    ) -> Option<Self> {
        self.try_steps(dir, steps)
            .filter(|&Self(x, y)| x <= x_max && y <= y_max)
            .filter(|&Self(x, y)| x_min <= x && y_min <= y)
    }
}

impl Position<usize> {
    pub fn try_steps_in_grid<U>(
        &self,
        dir: Direction,
        steps: impl Into<usize>,
        grid: &Grid<U>,
    ) -> Option<Self> {
        self.try_steps_with_upper_bounds(dir, steps.into(), grid.rows() - 1, grid.cols() - 1)
    }

    pub fn try_step_in_grid<U>(&self, dir: Direction, grid: &Grid<U>) -> Option<Self> {
        self.try_steps_in_grid(dir, 1_u8, grid)
    }
}

macro_rules! distance_functions {
    ($T_signed: ty, $T_unsigned: ty) => {
        impl Position<$T_signed> {
            pub const fn distance(&self, other: &Self) -> $T_unsigned {
                self.0.abs_diff(other.0) + self.1.abs_diff(other.1)
            }
        }
        impl Position<$T_unsigned> {
            pub const fn distance(&self, other: &Self) -> $T_unsigned {
                self.0.abs_diff(other.0) + self.1.abs_diff(other.1)
            }
        }
    };
}
distance_functions!(i8, u8);
distance_functions!(i16, u16);
distance_functions!(i32, u32);
distance_functions!(i64, u64);
distance_functions!(i128, u128);
distance_functions!(isize, usize);
