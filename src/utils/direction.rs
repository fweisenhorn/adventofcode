pub const NEIGHBOURS: [Direction; 4] = [
    Direction::Up,
    Direction::Down,
    Direction::Left,
    Direction::Right,
];

#[derive(PartialOrd, Ord, PartialEq, Eq, Hash, Copy, Clone, Debug)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl TryFrom<char> for Direction {
    type Error = &'static str;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'U' | 'u' | '^' => Ok(Self::Up),
            'D' | 'd' | 'v' => Ok(Self::Down),
            'L' | 'l' | '<' => Ok(Self::Left),
            'R' | 'r' | '>' => Ok(Self::Right),
            _ => Err("Direction cannot be created from char: {value:?}!"),
        }
    }
}

impl TryFrom<u8> for Direction {
    type Error = &'static str;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            b'U' | b'u' | b'^' => Ok(Self::Up),
            b'D' | b'd' | b'v' => Ok(Self::Down),
            b'L' | b'l' | b'<' => Ok(Self::Left),
            b'R' | b'r' | b'>' => Ok(Self::Right),
            _ => Err("Direction cannot be created from char: {value:?}!"),
        }
    }
}

impl Direction {
    pub const fn is_horizontal(self) -> bool {
        matches!(self, Self::Left | Self::Right)
    }

    pub const fn is_vertical(self) -> bool {
        matches!(self, Self::Up | Self::Down)
    }

    pub const fn as_number(self) -> usize {
        match self {
            Self::Up => 0,
            Self::Left => 1,
            Self::Down => 2,
            Self::Right => 3,
        }
    }

    pub const fn turn_right(self) -> Self {
        match self {
            Self::Up => Self::Right,
            Self::Down => Self::Left,
            Self::Left => Self::Up,
            Self::Right => Self::Down,
        }
    }

    pub const fn turn_left(self) -> Self {
        match self {
            Self::Up => Self::Left,
            Self::Down => Self::Right,
            Self::Left => Self::Down,
            Self::Right => Self::Up,
        }
    }

    pub const fn turn_around(self) -> Self {
        match self {
            Self::Up => Self::Down,
            Self::Down => Self::Up,
            Self::Left => Self::Right,
            Self::Right => Self::Left,
        }
    }

    pub fn turn_assign_right(&mut self) {
        *self = self.turn_right();
    }

    pub fn turn_assign_left(&mut self) {
        *self = self.turn_left();
    }

    pub fn turn_assign_around(&mut self) {
        *self = self.turn_around();
    }

    pub const fn mirror_slash(self) -> Self {
        match self {
            Self::Up => Self::Right,
            Self::Down => Self::Left,
            Self::Left => Self::Down,
            Self::Right => Self::Up,
        }
    }

    pub const fn mirror_backslash(self) -> Self {
        match self {
            Self::Up => Self::Left,
            Self::Down => Self::Right,
            Self::Left => Self::Up,
            Self::Right => Self::Down,
        }
    }
}
