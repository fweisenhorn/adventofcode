use std::hash::Hash;

pub mod direction;
pub mod grid;
pub mod position;

// Fast Hash structures
pub type HashMap<K, V> = rustc_hash::FxHashMap<K, V>;
pub type HashSet<T> = rustc_hash::FxHashSet<T>;

pub fn vec_freq_count<T: Hash + Eq + Clone>(v: &[T]) -> HashMap<T, usize> {
    iter_freq_counts(v.iter().cloned())
}

pub fn iter_freq_counts<T>(it: T) -> HashMap<T::Item, usize>
where
    T: Iterator + Sized,
    T::Item: Hash + Eq,
{
    let mut counts: HashMap<T::Item, usize> = HashMap::default();
    it.for_each(|item| *counts.entry(item).or_default() += 1);
    counts
}

pub const fn concat_ints(a: u64, b: u64) -> u64 {
    10_u64.pow(b.ilog10() + 1) * a + b
}

#[allow(unused_macros)]
macro_rules! find_min {
    // Base case:
    ($x:expr) => ($x);
    // `$x` followed by at least one `$y,`
    ($x:expr, $($y:expr),+) => (
        // Call `find_min!` on the tail `$y`
        std::cmp::min($x, find_min!($($y),+))
    )
}

macro_rules! find_max {
    // Base case:
    ($x:expr) => ($x);
    // `$x` followed by at least one `$y,`
    ($x:expr, $($y:expr),+) => (
        // Call `find_min!` on the tail `$y`
        std::cmp::max($x, find_max!($($y),+))
    )
}
