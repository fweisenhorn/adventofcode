use crate::utils::{direction::Direction, position::Position, HashMap, HashSet};

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day03.txt");

pub fn run() -> (u32, u32) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u32 {
    let data: Vec<Vec<char>> = input.lines().map(|line| line.chars().collect()).collect();

    let mut s: Vec<u32> = vec![];

    for (i, line) in data.iter().enumerate() {
        let mut cur: (u32, bool) = (0, false);

        for (j, c) in line.iter().enumerate() {
            if c.is_numeric() {
                // current char is part of a number
                cur.0 = cur.0 * 10 + c.to_digit(10).unwrap();

                // check if any neighbour is a punctuation mark (a)
                neighbours()
                    .iter()
                    .filter_map(|v| {
                        v.iter().try_fold(Pos::new(i, j), |acc, &dir| {
                            acc.try_steps_with_upper_bounds(dir, 1, data.len(), data[0].len())
                        })
                    })
                    .for_each(|p| {
                        if let Some(line) = data.get(p.0) {
                            if let Some(c) = line.get(p.1) {
                                if c.is_ascii_punctuation() && *c != '.' {
                                    cur.1 = true;
                                }
                            }
                        }
                    });
            } else if cur.0 != 0 {
                // current char isn't part of a number
                if cur.1 {
                    s.push(cur.0);
                }
                cur = (0, false);
            }
        }
        // at end of line, store current number
        if cur.0 != 0 && cur.1 {
            s.push(cur.0);
        }
    }

    s.into_iter().sum()
}

fn part_b(input: &str) -> u32 {
    let data: Vec<Vec<char>> = input.lines().map(|line| line.chars().collect()).collect();

    let mut t: HashMap<Pos, Vec<u32>> = HashMap::default();

    for (i, line) in data.iter().enumerate() {
        let mut cur: (u32, HashSet<Pos>) = (0, HashSet::default());

        for (j, c) in line.iter().enumerate() {
            if c.is_numeric() {
                // current char is part of a number
                cur.0 = cur.0 * 10 + c.to_digit(10).unwrap();

                // check if any neighbour is a punctuation mark (a) or '*' (b)
                neighbours()
                    .iter()
                    .filter_map(|v| {
                        v.iter().try_fold(Pos::new(i, j), |acc, &dir| {
                            acc.try_steps_with_upper_bounds(dir, 1, data.len(), data[0].len())
                        })
                    })
                    .for_each(|p| {
                        if let Some(line) = data.get(p.0) {
                            if let Some(c) = line.get(p.1) {
                                if *c == '*' {
                                    cur.1.insert(p);
                                }
                            }
                        }
                    });
            } else if cur.0 != 0 {
                // current char isn't part of a number
                if !cur.1.is_empty() {
                    cur.1.iter().for_each(|p| {
                        t.entry(*p)
                            .and_modify(|x| x.push(cur.0))
                            .or_insert_with(|| vec![cur.0]);
                    });
                }
                cur = (0, HashSet::default());
            }
        }
        // at end of line, store current number
        if cur.0 != 0 && !cur.1.is_empty() {
            cur.1.iter().for_each(|p| {
                t.entry(*p)
                    .and_modify(|x| x.push(cur.0))
                    .or_insert_with(|| vec![cur.0]);
            });
        }
    }

    t.into_values()
        .filter_map(|v| {
            if v.len() == 2 {
                Some(v.iter().product::<u32>())
            } else {
                None
            }
        })
        .sum()
}

fn neighbours() -> [Vec<Direction>; 8] {
    [
        vec![Direction::Up, Direction::Left],
        vec![Direction::Up],
        vec![Direction::Up, Direction::Right],
        vec![Direction::Left],
        vec![Direction::Right],
        vec![Direction::Down, Direction::Left],
        vec![Direction::Down],
        vec![Direction::Down, Direction::Right],
    ]
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day03.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 4361);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 530849);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 467835);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 84900879);
    }
}
