use crate::utils::HashMap;

const INPUT: &str = include_str!("inputs/day08.txt");

pub fn run() -> (u64, u64) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u64 {
    let input = input.split_once("\n\n").unwrap();

    let mut dirs = input.0.chars().cycle();

    let choices_map: HashMap<&str, [&str; 2]> = input
        .1
        .lines()
        .map(|s| (&s[0..3], [&s[7..10], &s[12..15]]))
        .collect::<HashMap<&str, [&str; 2]>>();

    let mut i = 0_u64;
    let mut cur_point = "AAA";

    loop {
        if cur_point == "ZZZ" {
            break;
        }

        i += 1;

        cur_point = match dirs.next() {
            Some('L') => choices_map[cur_point][0],
            Some('R') => choices_map[cur_point][1],
            _ => unreachable!(),
        }
    }
    i
}

fn part_b(input: &str) -> u64 {
    let input = input.split_once("\n\n").unwrap();

    let dirs = input.0.chars().cycle();

    let choices_map: HashMap<&str, [&str; 2]> = input
        .1
        .lines()
        .map(|s| (&s[0..3], [&s[7..10], &s[12..15]]))
        .collect::<HashMap<&str, [&str; 2]>>();

    let starting_points: Vec<&str> = choices_map
        .keys()
        .filter(|s| s.ends_with('A'))
        .copied()
        .collect();

    starting_points
        .iter()
        .map(|starting_point| {
            let mut i = 0_u64;
            let mut d = dirs.clone();
            let mut cur_point = *starting_point;

            loop {
                if cur_point.ends_with('Z') {
                    break;
                }

                i += 1;

                cur_point = match d.next() {
                    Some('L') => choices_map[cur_point][0],
                    Some('R') => choices_map[cur_point][1],
                    _ => unreachable!(),
                };
            }
            i
        })
        .fold(1_u64, lcm)
}

#[inline]
const fn gcd(mut a: u64, mut b: u64) -> u64 {
    while b != 0 {
        (a, b) = (b, a % b);
    }
    a
}

#[inline]
const fn lcm(a: u64, b: u64) -> u64 {
    a * b / gcd(a, b)
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST_A: &str = include_str!("tests/day08-a.txt");
    const TEST_B: &str = include_str!("tests/day08-b.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST_A), 6);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 11567);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST_B), 6);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 9858474970153);
    }
}
