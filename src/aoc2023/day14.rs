use crate::utils::{direction::Direction, grid::grid_from_input, position::Position, HashMap};
use grid::Grid;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day14.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let mut grid: Grid<char> = grid_from_input(input, |c| c);

    tilt_in_direction(&mut grid, Direction::Up);

    calculate_answer(&grid)
}

fn part_b(input: &str) -> usize {
    let mut grid: Grid<char> = grid_from_input(input, |c| c);

    let spin_cycles = 1_000_000_000_u32;
    let mut seen = HashMap::default();

    for i in 0..spin_cycles {
        if let Some(seen_at) = seen.insert(grid.clone(), i) {
            if (spin_cycles - i) % (i - seen_at) == 0 {
                break;
            }
        }

        tilt_in_direction(&mut grid, Direction::Up);
        tilt_in_direction(&mut grid, Direction::Left);
        tilt_in_direction(&mut grid, Direction::Down);
        tilt_in_direction(&mut grid, Direction::Right);
    }

    calculate_answer(&grid)
}

// TODO: Optimize
fn tilt_in_direction(grid: &mut Grid<char>, dir: Direction) {
    let max_ = (grid.rows() - 1, grid.cols() - 1);

    let mut has_changed = true;

    while has_changed {
        has_changed = false;

        (0..=max_.0).for_each(|i| {
            (0..=max_.1).for_each(|j| {
                let p = Pos::new(i, j);
                if let Some(new_p) = p.try_steps_with_upper_bounds(dir, 1, max_.0, max_.1) {
                    if grid[p] == 'O' && grid[new_p] == '.' {
                        grid[p] = '.';
                        grid[new_p] = 'O';
                        has_changed = true;
                    };
                }
            });
        });
    }
}

fn calculate_answer(grid: &Grid<char>) -> usize {
    let max_x = grid.rows();
    grid.indexed_iter()
        .filter(|(_, &c)| c == 'O')
        .map(|((i, _), _)| max_x - i)
        .sum()
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day14.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 136);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 109638);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 64);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 102657);
    }
}
