const INPUT: &str = include_str!("inputs/day09.txt");

pub fn run() -> (i32, i32) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> i32 {
    input
        .lines()
        .map(|line| {
            let v: Vec<i32> = line
                .split_whitespace()
                .map(|n| n.parse::<i32>().expect("Input should parse."))
                .collect();

            v.last().unwrap() + calc_next(&vec_diff(&v))
        })
        .sum()
}

fn part_b(input: &str) -> i32 {
    input
        .lines()
        .map(|line| {
            let v: Vec<i32> = line
                .split_whitespace()
                .map(|n| n.parse::<i32>().expect("Input should parse."))
                .collect();

            v.first().unwrap() - calc_prev(&vec_diff(&v))
        })
        .sum()
}

fn vec_diff(v: &[i32]) -> Vec<i32> {
    let vals = v.iter();
    let next_vals = v.iter().skip(1);

    vals.zip(next_vals).map(|(cur, next)| next - cur).collect()
}

fn calc_next(v: &[i32]) -> i32 {
    if vec_diff(v).iter().all(|&x| x == 0) {
        *v.last().unwrap()
    } else {
        *v.last().unwrap() + calc_next(&vec_diff(v))
    }
}

fn calc_prev(v: &[i32]) -> i32 {
    if vec_diff(v).iter().all(|&x| x == 0) {
        *v.first().unwrap()
    } else {
        *v.first().unwrap() - calc_prev(&vec_diff(v))
    }
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day09.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 114);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 1868368343);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 2);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 1022);
    }
}
