use crate::utils::position::Position;
use crate::utils::HashSet;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day13.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    input.split("\n\n").fold(0, |acc, s| {
        let rocks = get_rocks_from_input(s);

        let max_x: usize = rocks.iter().map(|p| p.0).max().unwrap();
        let max_y: usize = rocks.iter().map(|p| p.1).max().unwrap();

        for mirror_pos in 1..max_x {
            if rocks
                .iter()
                .filter(|rock| {
                    (rock.0 <= mirror_pos && 2 * mirror_pos < max_x + rock.0)
                        || (rock.0 > mirror_pos && 2 * mirror_pos >= rock.0)
                })
                .all(|rock| rocks.contains(&Pos::new(2 * mirror_pos + 1 - rock.0, rock.1)))
            {
                return acc + mirror_pos * 100;
            }
        }

        for mirror_pos in 1..max_y {
            if rocks
                .iter()
                .filter(|rock| {
                    (rock.1 <= mirror_pos && 2 * mirror_pos < max_y + rock.1)
                        || (rock.1 > mirror_pos && 2 * mirror_pos >= rock.1)
                })
                .all(|rock| rocks.contains(&Pos::new(rock.0, 2 * mirror_pos + 1 - rock.1)))
            {
                return acc + mirror_pos;
            }
        }

        unreachable!()
    })
}

fn part_b(input: &str) -> usize {
    input.split("\n\n").fold(0, |acc, s| {
        let rocks = get_rocks_from_input(s);

        let max_x: usize = rocks.iter().map(|p| p.0).max().unwrap();
        let max_y: usize = rocks.iter().map(|p| p.1).max().unwrap();

        for mirror_pos in 1..max_x {
            if rocks
                .iter()
                .filter(|rock| {
                    (rock.0 <= mirror_pos && 2 * mirror_pos < max_x + rock.0)
                        || (rock.0 > mirror_pos && 2 * mirror_pos >= rock.0)
                })
                .filter(|rock| !rocks.contains(&Pos::new(2 * mirror_pos + 1 - rock.0, rock.1)))
                .count()
                == 1
            {
                return acc + mirror_pos * 100;
            }
        }

        for mirror_pos in 1..max_y {
            if rocks
                .iter()
                .filter(|rock| {
                    (rock.1 <= mirror_pos && 2 * mirror_pos < max_y + rock.1)
                        || (rock.1 > mirror_pos && 2 * mirror_pos >= rock.1)
                })
                .filter(|rock| !rocks.contains(&Pos::new(rock.0, 2 * mirror_pos + 1 - rock.1)))
                .count()
                == 1
            {
                return acc + mirror_pos;
            }
        }

        unreachable!()
    })
}

fn get_rocks_from_input(input: &str) -> HashSet<Pos> {
    let mut rocks = HashSet::default();
    input.lines().enumerate().for_each(|(i, line)| {
        line.chars().enumerate().for_each(|(j, c)| {
            if c == '#' {
                rocks.insert(Pos::new(i + 1, j + 1));
            };
        });
    });
    rocks
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day13.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 405);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 37561);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 400);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 31108);
    }
}
