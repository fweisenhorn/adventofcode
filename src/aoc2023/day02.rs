use std::cmp::max;

const INPUT: &str = include_str!("inputs/day02.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    parse_input(input)
        .into_iter()
        .enumerate()
        .filter(|(_, game)| game.iter().all(|&[r, g, b]| r <= 12 && g <= 13 && b <= 14))
        .map(|(i, _)| i + 1)
        .sum()
}

pub fn part_b(input: &str) -> usize {
    parse_input(input)
        .into_iter()
        .map(|line| {
            let t = line
                .into_iter()
                .reduce(|acc, e| [max(acc[0], e[0]), max(acc[1], e[1]), max(acc[2], e[2])])
                .unwrap();
            t[0] * t[1] * t[2]
        })
        .sum()
}

type Hand = [usize; 3];

fn parse_input(input: &str) -> Vec<Vec<Hand>> {
    input
        .lines()
        .map(|line| {
            let (_, t) = line.split_once(": ").unwrap();
            t.split("; ")
                .map(|round| {
                    round
                        .split(", ")
                        .map(|s| {
                            let (a, b) = s.split_once(' ').unwrap();
                            let a = a.parse::<usize>().unwrap();
                            match b {
                                "red" => [a, 0, 0],
                                "green" => [0, a, 0],
                                "blue" => [0, 0, a],
                                _ => unreachable!(),
                            }
                        })
                        .reduce(|acc, e| [acc[0] + e[0], acc[1] + e[1], acc[2] + e[2]])
                        .unwrap()
                })
                .collect()
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day02.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 8);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 3059);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 2286);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 65371);
    }
}
