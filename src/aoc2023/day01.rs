const INPUT: &str = include_str!("inputs/day01.txt");

const NUMBERS: [&str; 9] = [
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

pub fn run() -> (u32, u32) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u32 {
    input
        .lines()
        .map(|line| {
            let a = line
                .matches(char::is_numeric)
                .next()
                .expect("Line should contain at least one digit.")
                .parse::<u32>()
                .unwrap();

            let b = line
                .rmatches(char::is_numeric)
                .next()
                .expect("Line should contain at least one digit.")
                .parse::<u32>()
                .unwrap();

            10 * a + b
        })
        .sum()
}

fn part_b(input: &str) -> u32 {
    input
        .lines()
        .map(|line| {
            let a = {
                let n = line
                    .find(char::is_numeric)
                    .expect("Line should contain at least one digit.");

                NUMBERS
                    .iter()
                    .filter_map(|s| line.find(s))
                    .min()
                    .filter(|&m| m < n)
                    .map_or_else(
                        || parse_num_in_line_at_idx(line, n).unwrap(),
                        |m| get_num_in_line_at_idx(line, m).unwrap(),
                    )
            };

            let b = {
                let n = line
                    .rfind(char::is_numeric)
                    .expect("Line should contain at least one digit.");

                NUMBERS
                    .iter()
                    .filter_map(|s| line.rfind(s))
                    .max()
                    .filter(|&m| m > n)
                    .map_or_else(
                        || parse_num_in_line_at_idx(line, n).unwrap(),
                        |m| get_num_in_line_at_idx(line, m).unwrap(),
                    )
            };

            10 * a + b
        })
        .sum()
}

fn parse_num_in_line_at_idx(line: &str, idx: usize) -> Option<u32> {
    line.get(idx..=idx)?.parse::<u32>().ok()
}

fn get_num_in_line_at_idx(line: &str, idx: usize) -> Option<u32> {
    match line.get(idx..=idx + 1)? {
        "on" => Some(1),
        "tw" => Some(2),
        "th" => Some(3),
        "fo" => Some(4),
        "fi" => Some(5),
        "si" => Some(6),
        "se" => Some(7),
        "ei" => Some(8),
        "ni" => Some(9),
        _ => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_A: &str = include_str!("tests/day01-a.txt");
    const TEST_B: &str = include_str!("tests/day01-b.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST_A), 142);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 54159);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST_B), 281);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 53866);
    }
}
