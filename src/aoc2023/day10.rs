use crate::utils::{
    direction::{Direction, NEIGHBOURS},
    grid::{find_in_grid, grid_from_input},
    position::Position,
    HashSet,
};
use grid::Grid;
use std::collections::VecDeque;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day10.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let m: Grid<char> = grid_from_input(input, |c| c);
    let start = find_in_grid(&'S', &m).unwrap();

    let mut i = 0;

    let mut cur_pos = start;
    let mut cur_dir = find_initial_direction(&m, &start);

    // go through the loop until we are back at the start
    loop {
        i += 1;

        if let Some(new_pos) = cur_pos.try_step_in_grid(cur_dir, &m) {
            cur_pos = new_pos;
            match (cur_dir, m[new_pos]) {
                (_, 'S') => {
                    break;
                }
                (Direction::Up | Direction::Down, '|')
                | (Direction::Left | Direction::Right, '-') => (),

                (Direction::Up, '7')
                | (Direction::Down, 'L')
                | (Direction::Left, 'F')
                | (Direction::Right, 'J') => {
                    cur_dir = cur_dir.turn_left();
                }
                (Direction::Up, 'F')
                | (Direction::Down, 'J')
                | (Direction::Left, 'L')
                | (Direction::Right, '7') => {
                    cur_dir = cur_dir.turn_right();
                }
                _ => unreachable!(),
            }
        }
    }

    i / 2
}

fn part_b(input: &str) -> usize {
    let m: Grid<char> = grid_from_input(input, |c| c);
    let start = find_in_grid(&'S', &m).unwrap();

    let mut cur_pos = start;
    let mut cur_dir = find_initial_direction(&m, &start);

    let mut visited: HashSet<Pos> = HashSet::default();
    let mut passed_rhs: HashSet<Pos> = HashSet::default();
    let mut passed_lhs: HashSet<Pos> = HashSet::default();

    let mut turn_counter: i32 = 0; // will be > 0 if the route was walked in a right turn

    while let Some(new_pos) = cur_pos.try_step_in_grid(cur_dir, &m) {
        visited.insert(new_pos);
        cur_pos = new_pos;

        match (cur_dir, m[new_pos]) {
            (_, 'S') => {
                break;
            }

            (Direction::Up | Direction::Down, '|') | (Direction::Left | Direction::Right, '-') => {
                add_neighbour(&cur_pos, cur_dir.turn_left(), &mut passed_lhs);
                add_neighbour(&cur_pos, cur_dir.turn_right(), &mut passed_rhs);
            }

            (Direction::Up, '7')
            | (Direction::Down, 'L')
            | (Direction::Left, 'F')
            | (Direction::Right, 'J') => {
                add_neighbour(&cur_pos, cur_dir, &mut passed_rhs);
                add_neighbour(&cur_pos, cur_dir.turn_right(), &mut passed_rhs);
                cur_dir = cur_dir.turn_left();
                turn_counter -= 1;
            }
            (Direction::Up, 'F')
            | (Direction::Down, 'J')
            | (Direction::Left, 'L')
            | (Direction::Right, '7') => {
                add_neighbour(&cur_pos, cur_dir, &mut passed_lhs);
                add_neighbour(&cur_pos, cur_dir.turn_left(), &mut passed_lhs);
                cur_dir = cur_dir.turn_right();
                turn_counter += 1;
            }

            _ => unreachable!(),
        }
    }

    expand_queue(
        (if turn_counter > 0 {
            passed_rhs
        } else {
            passed_lhs
        })
        .difference(&visited)
        .copied()
        .collect(),
        &visited,
        &m,
    )
    .len()
}

fn find_initial_direction(m: &Grid<char>, start: &Pos) -> Direction {
    if let Some(t) = start.try_step_in_grid(Direction::Down, m) {
        if matches!(m[t], '|' | 'J' | 'L') {
            return Direction::Down;
        }
    }

    if let Some(t) = start.try_step_in_grid(Direction::Up, m) {
        if matches!(m[t], '|' | 'F' | '7') {
            return Direction::Up;
        }
    }

    if let Some(t) = start.try_step_in_grid(Direction::Right, m) {
        if matches!(m[t], '-' | 'J' | '7') {
            return Direction::Right;
        }
    }

    unreachable!()
}

fn add_neighbour(cur_pos: &Pos, d: Direction, s: &mut HashSet<Pos>) {
    if let Some(neigh) = cur_pos.try_steps(d, 1) {
        s.insert(neigh);
    }
}

fn expand_queue(
    mut to_expand: VecDeque<Pos>,
    visited: &HashSet<Pos>,
    m: &Grid<char>,
) -> HashSet<Pos> {
    let mut expanded = HashSet::<Pos>::default();

    while let Some(cur) = to_expand.pop_front() {
        NEIGHBOURS
            .iter()
            .filter_map(|&d| cur.try_step_in_grid(d, m))
            .for_each(|pos| {
                if !(expanded.contains(&pos) || to_expand.contains(&pos) || visited.contains(&pos))
                {
                    to_expand.push_back(pos);
                }
            });

        expanded.insert(cur);
    }

    expanded
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_A1: &str = include_str!("tests/day10-a1.txt");
    const TEST_A2: &str = include_str!("tests/day10-a2.txt");

    const TEST_B1: &str = include_str!("tests/day10-b1.txt");
    const TEST_B2: &str = include_str!("tests/day10-b2.txt");
    const TEST_B3: &str = include_str!("tests/day10-b3.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST_A1), 4);
        assert_eq!(part_a(TEST_A2), 8);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 6714);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST_B1), 4);
        assert_eq!(part_b(TEST_B2), 8);
        assert_eq!(part_b(TEST_B3), 10);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 429);
    }
}
