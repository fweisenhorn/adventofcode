use crate::utils::position::Position;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day11.txt");

pub fn run() -> (usize, usize) {
    (expand_space(INPUT, 2), expand_space(INPUT, 1_000_000))
}

fn expand_space(input: &str, expansion: usize) -> usize {
    let mut galaxies: Vec<Pos> = vec![];

    input.lines().enumerate().for_each(|(i, line)| {
        line.chars().enumerate().for_each(|(j, c)| {
            if c == '#' {
                galaxies.push(Pos::new(i, j));
            };
        });
    });

    let max_x = input.lines().count();
    let max_y = input.lines().next().unwrap().chars().count();

    for x_ in (0..max_x).rev() {
        if !galaxies.iter().any(|&p| p.0 == x_) {
            galaxies.iter_mut().filter(|p| p.0 > x_).for_each(|p| {
                p.0 += expansion - 1;
            });
        }
    }

    for y_ in (0..max_y).rev() {
        if !galaxies.iter().any(|&p| p.1 == y_) {
            galaxies.iter_mut().filter(|p| p.1 > y_).for_each(|p| {
                p.1 += expansion - 1;
            });
        }
    }

    let mut x = 0_usize;

    for gal1 in &galaxies {
        for gal2 in &galaxies {
            x += gal1.distance(gal2);
        }
    }

    x / 2
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day11.txt");

    #[test]
    fn test_a() {
        assert_eq!(expand_space(TEST, 2), 374);
    }

    #[test]
    fn result_a() {
        assert_eq!(expand_space(INPUT, 2), 9543156);
    }

    #[test]
    fn test_b() {
        assert_eq!(expand_space(TEST, 10), 1030);
        assert_eq!(expand_space(TEST, 100), 8410);
    }

    #[test]
    fn result_b() {
        assert_eq!(expand_space(INPUT, 1_000_000), 625243292686);
    }
}
