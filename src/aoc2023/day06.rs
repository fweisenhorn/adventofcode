use crate::utils::concat_ints;

const INPUT: &str = include_str!("inputs/day06.txt");

pub fn run() -> (usize, u64) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let (times, dists) = parse_input(input);

    times
        .into_iter()
        .zip(dists)
        .map(|(t, d)| (0..t).filter(|i| (t - i) * i > d).count())
        .product()
}

fn part_b(input: &str) -> u64 {
    let (times, dists) = parse_input(input);

    let time = concat_ints_in_line(&times);
    let dist = concat_ints_in_line(&dists);

    min_and_max_solutions(time, dist).expect("Should find solutions to quadratic equations")
}

fn concat_ints_in_line(v: &[u32]) -> u64 {
    v.iter()
        .fold(0_u64, |acc, &x| concat_ints(acc, u64::from(x)))
}

fn parse_input(input: &str) -> (Vec<u32>, Vec<u32>) {
    let mut t = input.lines();
    let a = parse_line(t.next().unwrap());
    let b = parse_line(t.next().unwrap());

    (a, b)
}

fn parse_line(line: &str) -> Vec<u32> {
    line.split_whitespace()
        .filter_map(|s| s.parse::<u32>().ok())
        .collect()
}

#[allow(
    clippy::cast_possible_truncation,
    clippy::cast_precision_loss,
    clippy::cast_sign_loss
)]
fn min_and_max_solutions(t: u64, d: u64) -> Option<u64> {
    let x = ((t * t).checked_sub(4 * d)? as f64).sqrt();

    let x1 = ((t as f64 - x) / 2.).ceil() as u64;
    let x2 = ((t as f64 + x) / 2.).floor() as u64;

    Some(x2 - x1 + 1)
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day06.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 288);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 4568778);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 71503);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 28973936);
    }
}
