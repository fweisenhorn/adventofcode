use crate::utils::{direction::Direction, grid::grid_from_input, position::Position};
use grid::Grid;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day16.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let grid: grid::Grid<char> = grid_from_input(input, |c| c);

    light_up_grid(&grid, Pos::new(0, 0), Direction::Right)
}

fn part_b(input: &str) -> usize {
    let grid: grid::Grid<char> = grid_from_input(input, |c| c);

    find_max!(
        (0..grid.cols())
            .map(|y_| light_up_grid(&grid, Pos::new(0, y_), Direction::Down))
            .max()
            .unwrap(),
        (0..grid.cols())
            .map(|y_| light_up_grid(&grid, Pos::new(grid.rows() - 1, y_), Direction::Down))
            .max()
            .unwrap(),
        (0..grid.rows())
            .map(|x_| light_up_grid(&grid, Pos::new(x_, 0), Direction::Right))
            .max()
            .unwrap(),
        (0..grid.rows())
            .map(|x_| light_up_grid(&grid, Pos::new(x_, grid.cols() - 1), Direction::Left))
            .max()
            .unwrap()
    )
}

fn light_up_grid(grid: &Grid<char>, starting_pos: Pos, starting_dir: Direction) -> usize {
    let mut energized: Grid<[bool; 4]> = Grid::init(grid.rows(), grid.cols(), [false; 4]);

    let mut beam_ends: Vec<(Pos, Direction)> = vec![(starting_pos, starting_dir)];

    while let Some((cur_pos, cur_dir)) = beam_ends.pop() {
        if energized[cur_pos][cur_dir.as_number()] {
            continue;
        }

        energized[cur_pos][cur_dir.as_number()] = true;

        match grid[cur_pos] {
            '/' => {
                push_pos_and_dir(&mut beam_ends, cur_pos, cur_dir.mirror_slash(), grid);
            }
            '\\' => {
                push_pos_and_dir(&mut beam_ends, cur_pos, cur_dir.mirror_backslash(), grid);
            }
            '-' if cur_dir.is_vertical() => {
                push_pos_and_dir(&mut beam_ends, cur_pos, Direction::Left, grid);
                push_pos_and_dir(&mut beam_ends, cur_pos, Direction::Right, grid);
            }
            '|' if cur_dir.is_horizontal() => {
                push_pos_and_dir(&mut beam_ends, cur_pos, Direction::Up, grid);
                push_pos_and_dir(&mut beam_ends, cur_pos, Direction::Down, grid);
            }
            _ => {
                push_pos_and_dir(&mut beam_ends, cur_pos, cur_dir, grid);
            }
        }
    }

    energized.iter().filter(|x| x.iter().any(|&b| b)).count()
}

#[inline]
fn push_pos_and_dir(
    queue: &mut Vec<(Pos, Direction)>,
    pos: Pos,
    dir: Direction,
    grid: &Grid<char>,
) {
    if let Some(new_pos) = pos.try_step_in_grid(dir, grid) {
        queue.push((new_pos, dir));
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day16.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 46);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 7415);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 51);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 7943);
    }
}
