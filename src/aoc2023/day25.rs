const INPUT: &str = include_str!("inputs/day25.txt");

pub fn run() -> (u32, String) {
    (part_a(INPUT), part_b(INPUT))
}

// TODO
const fn part_a(_input: &str) -> u32 {
    0
}

fn part_b(_input: &str) -> String {
    '🎄'.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day25.txt");

    #[test]
    #[ignore = "not yet implemented"]
    fn test() {
        assert_eq!(part_a(TEST), 0);
    }

    #[test]
    #[ignore = "not yet implemented"]
    fn result() {
        assert_eq!(part_a(INPUT), 0);
    }
}
