use crate::utils::{
    direction::NEIGHBOURS,
    grid::{find_in_grid, grid_from_input},
    position::Position,
    HashSet,
};

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day21.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT, 64), part_b(INPUT, 26_501_365))
}

fn part_a(input: &str, n_steps: usize) -> usize {
    let grid = grid_from_input(input, |c| c);
    let start_pos = find_in_grid(&'S', &grid).unwrap();
    let grid = grid_from_input(input, |c| c != '#');

    let mut possible_pos = HashSet::<Pos>::default();
    possible_pos.insert(start_pos);

    for _ in 0..n_steps {
        let mut next_possible_pos = vec![];
        for pos in &possible_pos {
            NEIGHBOURS
                .into_iter()
                .filter_map(|d| pos.try_step_in_grid(d, &grid))
                .for_each(|p| {
                    if grid[p] {
                        next_possible_pos.push(p);
                    }
                });
        }

        possible_pos = next_possible_pos.into_iter().collect();
    }

    possible_pos.len()
}

// XXX: This solution takes a shortcut that I can't quite explain
fn part_b(_input: &str, n_steps: usize) -> usize {
    // let f = |x: usize| -> usize { x * 131 * 2 + 65 };

    // let fx = part_b_testing(_input, &[f(0), f(1), f(2)]);

    // assert_eq!(fx, vec![3784, 93_366, 302_108]);

    // let c = fx[0];
    // let a = ((fx[2] - c) - 2 * (fx[1] - c)) / 2;
    // let b = fx[1] - a - c;

    // assert_eq!(a, 59580);
    // assert_eq!(b, 30002);
    // assert_eq!(c, 3784);

    let a = 59580;
    let b = 30002;
    let c = 3784;

    let f = |x: usize| -> usize { a * x * x + b * x + c };

    f((n_steps) / (131 * 2))
}

#[allow(clippy::cast_possible_wrap, dead_code)]
fn part_b_testing(input: &str, n_steps: &[usize]) -> Vec<usize> {
    let grid = grid_from_input(input, |c| c);
    let start_pos = find_in_grid(&'S', &grid).unwrap();
    let start_pos: Position<isize> = Position(start_pos.0 as isize, start_pos.1 as isize);
    let grid = grid_from_input(input, |c| c != '#');

    let mut out = vec![];

    let mut possible_pos = vec![start_pos];
    for i in 0..*n_steps.last().unwrap() {
        if n_steps.contains(&i) {
            out.push(possible_pos.len());
        }

        let mut next_possible_pos = vec![];

        for pos in &possible_pos {
            for &d in &NEIGHBOURS {
                let new_p = pos.steps_unchecked(d, 1);

                if grid[(
                    new_p.0.rem_euclid(131) as usize,
                    new_p.1.rem_euclid(131) as usize,
                )] {
                    next_possible_pos.push(new_p);
                }
            }
        }

        next_possible_pos.sort_unstable();
        next_possible_pos.dedup();

        possible_pos = next_possible_pos;
    }

    out.push(possible_pos.len());
    out
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day21.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST, 6), 16);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT, 64), 3677);
    }

    #[test]
    #[ignore = "takes too long"]
    fn test_b() {
        assert_eq!(
            part_b_testing(INPUT, &[65, 131 * 2 + 65, 131 * 4 + 65]),
            vec![3784, 93366, 302108]
        );
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT, 26_501_365), 609585229256084);
    }
}
