use std::collections::VecDeque;

const INPUT: &str = include_str!("inputs/day04.txt");

pub fn run() -> (u32, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u32 {
    input
        .lines()
        .map(parse_wins_per_line)
        .map(|x| match x {
            0 => 0,
            n => 2_u32.pow(n.try_into().unwrap()) / 2,
        })
        .sum()
}

fn part_b(input: &str) -> usize {
    input
        .lines()
        .map(parse_wins_per_line)
        .fold(
            (0_usize, VecDeque::<usize>::new()),
            |mut acc: (usize, VecDeque<usize>), x| {
                let cur = 1 + acc.1.pop_front().unwrap_or(0);
                acc.0 += cur;
                for k in 0..x {
                    match acc.1.get_mut(k) {
                        Some(q) => *q += cur,
                        None => acc.1.push_back(cur),
                    }
                }
                acc
            },
        )
        .0
}

fn parse_wins_per_line(line: &str) -> usize {
    let (_, a) = line.split_once(": ").unwrap();
    let (a, b) = a.split_once(" | ").unwrap();
    let lhs_nums: Vec<u32> = a
        .split_whitespace()
        .filter_map(|x| x.parse::<u32>().ok())
        .collect();
    let rhs_nums: Vec<u32> = b
        .split_whitespace()
        .filter_map(|x| x.parse::<u32>().ok())
        .collect();

    lhs_nums.iter().filter(|x| rhs_nums.contains(x)).count()
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day04.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 13);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 23235);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 30);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 5920640);
    }
}
