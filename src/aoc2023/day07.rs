use crate::utils::HashMap;
use std::cmp::Ordering;
use std::ops::AddAssign;

const INPUT: &str = include_str!("inputs/day07.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let mut hands = read_input(input, DeckType::StandardDeck).expect("Input should parse");
    hands.sort_unstable_by_key(|&(h, _)| h);
    count_result(&hands)
}

fn part_b(input: &str) -> usize {
    let mut hands = read_input(input, DeckType::JokerDeck).expect("Input should parse");
    hands.sort_unstable_by_key(|&(h, _)| h);
    count_result(&hands)
}

fn read_input(input: &str, d: DeckType) -> Option<Vec<(Hand, usize)>> {
    input
        .lines()
        .map(|line| {
            let (a, n) = line.split_at(5);
            Some((Hand::new(a, d)?, n[1..].parse().ok()?))
        })
        .collect()
}

fn count_result(hands: &[(Hand, usize)]) -> usize {
    hands
        .iter()
        .enumerate()
        .fold(0, |acc, (i, &(_, x))| acc + (i + 1) * x)
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
enum DeckType {
    StandardDeck,
    JokerDeck,
}

impl DeckType {
    fn card_value(self, x: char) -> Option<u8> {
        match self {
            Self::StandardDeck => [
                '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A',
            ],
            Self::JokerDeck => [
                'J', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'Q', 'K', 'A',
            ],
        }
        .into_iter()
        .position(|t| t == x)
        .and_then(|x| u8::try_from(x).ok())
    }

    fn occurrences(self, cards: &[u8]) -> [u8; 2] {
        let mut occ: HashMap<&u8, u8> = cards.iter().fold(HashMap::default(), |mut acc, c| {
            acc.entry(c).or_insert(0).add_assign(1);
            acc
        });

        let j = match self {
            Self::StandardDeck => 0,
            Self::JokerDeck => occ.remove(&self.card_value('J').unwrap()).unwrap_or(0),
        };

        if j == 5 {
            return [5, 0];
        }

        let mut t: Vec<_> = occ.into_values().collect();

        t.sort_unstable();

        match (t.pop(), t.pop()) {
            (Some(_), None) => [5, 0],
            (Some(x), Some(y)) => [x + j, y],
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
struct Hand {
    values: [u8; 7],
    deck_type: DeckType,
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        self.values
            .iter()
            .zip(other.values)
            .map(|(a, b)| a.cmp(&b))
            .find(|&x| x != Ordering::Equal)
            .unwrap_or(Ordering::Equal)
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Hand {
    fn new(s: &str, d: DeckType) -> Option<Self> {
        assert_eq!(s.len(), 5);

        let t: Vec<u8> = s.chars().map(|x| d.card_value(x)).collect::<Option<_>>()?;

        assert_eq!(t.len(), 5);

        Some(Self {
            values: d
                .occurrences(&t)
                .into_iter()
                .chain(t)
                .collect::<Vec<_>>()
                .try_into()
                .unwrap(),
            deck_type: d,
        })
    }
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day07.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 6440);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 250347426);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 5905);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 251224870);
    }
}
