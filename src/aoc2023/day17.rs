use crate::utils::{direction::Direction, grid::grid_from_input, position::Position, HashMap};
use grid::Grid;
use std::{cmp::Reverse, collections::BinaryHeap};

type Pos = Position<usize>;
type HeapKey = (Reverse<u32>, Pos, Direction);

const INPUT: &str = include_str!("inputs/day17.txt");

pub fn run() -> (u32, u32) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u32 {
    dijkstra(input, 1, 3)
}

fn part_b(input: &str) -> u32 {
    dijkstra(input, 4, 10)
}

fn dijkstra(input: &str, min_step: u8, max_step: u8) -> u32 {
    let heat_map = grid_from_input(input, |c| c.to_digit(10).unwrap());

    let max_x = heat_map.rows();
    let max_y = heat_map.cols();

    let mut queue: BinaryHeap<HeapKey> = BinaryHeap::new();
    let mut cache: HashMap<(Pos, Direction), u32> = HashMap::default();

    queue.push((Reverse(0), Pos::new(0, 0), Direction::Right));
    queue.push((Reverse(0), Pos::new(0, 0), Direction::Down));

    while let Some(x) = queue.pop() {
        let (Reverse(weight), pos, _) = x;

        if pos == (Pos::new(max_x - 1, max_y - 1)) {
            return weight;
        }

        expand_if_able(&mut queue, &mut cache, x, &heat_map, min_step, max_step);
    }

    unreachable!()
}

fn expand_if_able(
    queue: &mut BinaryHeap<HeapKey>,
    cache: &mut HashMap<(Pos, Direction), u32>,
    heapkey: HeapKey,
    heat_map: &Grid<u32>,
    min_step: u8,
    max_step: u8,
) {
    let (Reverse(cur_weight), cur_pos, cur_dir) = heapkey;

    if cache
        .get(&(cur_pos, cur_dir))
        .is_some_and(|&t| cur_weight > t)
    {
        return;
    }

    for new_dir in [cur_dir.turn_left(), cur_dir.turn_right()] {
        let mut new_weight = cur_weight;

        for n in 1..=max_step {
            if let Some(new_pos) = cur_pos.try_steps_in_grid(new_dir, n, heat_map) {
                new_weight += heat_map[new_pos];

                if n < min_step {
                    continue;
                }

                if cache
                    .get(&(new_pos, new_dir))
                    .is_none_or(|&t| new_weight < t)
                {
                    cache.insert((new_pos, new_dir), new_weight);
                    queue.push((Reverse(new_weight), new_pos, new_dir));
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day17.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 102);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 902);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 94);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 1073);
    }
}
