use nalgebra::{Matrix6, Vector3, Vector6};
use std::ops::Mul;

const INPUT: &str = include_str!("inputs/day24.txt");

struct Hailstone {
    p: Vector3<f64>,
    v: Vector3<f64>,
}

pub fn run() -> (usize, u64) {
    (
        part_a(INPUT, 200_000_000_000_000., 400_000_000_000_000.),
        part_b(INPUT),
    )
}

fn part_a(input: &str, lower_bound: f64, upper_bound: f64) -> usize {
    let hailstones: Vec<Hailstone> = read_input(input);

    let mut crossings_counter = 0;

    for i in 0..hailstones.len() {
        for j in 0..i {
            let h1 = &hailstones[i];
            let h2 = &hailstones[j];

            let denom = h1.v[0].mul_add(h2.v[1], -h1.v[1] * h2.v[0]);

            let u = (h1.p[0] - h2.p[0]).mul_add(-h1.v[1], (h1.p[1] - h2.p[1]) * (h1.v[0])) / denom;
            if !(0. <= u && u.is_normal()) {
                continue;
            }

            let t = (h1.p[0] - h2.p[0]).mul_add(-h2.v[1], (h1.p[1] - h2.p[1]) * (h2.v[0])) / denom;

            if check_bounds(t, 0., f64::MAX)
                && check_bounds(t.mul_add(h1.v[0], h1.p[0]), lower_bound, upper_bound)
                && check_bounds(t.mul_add(h1.v[1], h1.p[1]), lower_bound, upper_bound)
            {
                crossings_counter += 1;
            }
        }
    }

    crossings_counter
}

#[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
fn part_b(input: &str) -> u64 {
    let hailstones = read_input(input);

    let h1 = &hailstones[0];
    let h2 = &hailstones[1];
    let h3 = &hailstones[2];

    assert_ne!(h1.v, h2.v);
    assert_ne!(h1.v, h3.v);
    assert_ne!(h2.v, h3.v);

    let rhs: Vector6<f64> = {
        let top = h2.p.cross(&h2.v) - h1.p.cross(&h1.v);
        let bot = h3.p.cross(&h3.v) - h1.p.cross(&h1.v);

        top.insert_fixed_rows::<3>(3, 0.) + bot.insert_fixed_rows::<3>(0, 0.)
    };
    let lhs: Matrix6<f64> = {
        let top_left = (h1.v - h2.v).cross_matrix();
        let top_right = (h1.p - h2.p).cross_matrix();
        let top =
            top_left.insert_fixed_columns::<3>(3, 0.) + top_right.insert_fixed_columns::<3>(0, 0.);

        let bot_left = (h1.v - h3.v).cross_matrix();
        let bot_right = (h1.p - h3.p).cross_matrix();
        let bot =
            bot_left.insert_fixed_columns::<3>(3, 0.) + bot_right.insert_fixed_columns::<3>(0, 0.);

        top.insert_fixed_rows::<3>(3, 0.) + bot.insert_fixed_rows::<3>(0, 0.)
    };

    lhs.try_inverse()
        .expect("Should be able to find a solution")
        .mul(rhs)
        .remove_fixed_rows::<3>(3)
        .sum()
        .ceil() as u64
}

fn read_input(input: &str) -> Vec<Hailstone> {
    input.lines().map(read_line).collect()
}

fn read_line(line: &str) -> Hailstone {
    let t = line
        .split(&[',', '@'])
        .map(|s| s.trim().parse())
        .collect::<Result<Vec<f64>, _>>()
        .expect("Input should parse");
    Hailstone {
        p: Vector3::from_column_slice(&t[..3]),
        v: Vector3::from_column_slice(&t[3..]),
    }
}

fn check_bounds(x: f64, lower_bound: f64, upper_bound: f64) -> bool {
    x.is_normal() && lower_bound <= x && x <= upper_bound
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day24.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST, 7., 27.), 2);
    }

    #[test]
    fn result_a() {
        assert_eq!(
            part_a(INPUT, 200_000_000_000_000., 400_000_000_000_000.),
            12783
        );
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 47);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 948485822969419);
    }
}
