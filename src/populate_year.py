def main():
    import os
    import sys

    with open("template.txt", 'r') as f:
        text = f.read()

    year = sys.argv[1]
    dirname = f"aoc{year}"

    assert os.path.isdir(dirname) == False
    os.mkdir(dirname)
    os.mkdir(dirname + "/tests")
    os.mkdir(dirname + "/inputs")
    
    for day in range(1, 26):
        day_s = f"{day:02d}"
        with open(dirname + f"/day{day_s}.rs", 'x') as f:
            f.write(text.replace("XX", day_s))
        with open(dirname + f"/inputs/day{day_s}.txt", 'x') as f:
            f.write("")
        with open(dirname + f"/tests/day{day_s}.txt", 'x') as f:
            f.write("")
    
        
if __name__ == "__main__":
    main()
