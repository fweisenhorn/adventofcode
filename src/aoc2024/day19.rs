use crate::utils::HashMap;
use rayon::prelude::*;

const INPUT: &str = include_str!("inputs/day19.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let (towels, patterns) = parse_input(input);

    patterns
        .par_iter()
        .filter(|pattern| is_buildable(&towels, pattern))
        .count()
}

fn part_b(input: &str) -> usize {
    let (towels, patterns) = parse_input(input);

    let mut cache = HashMap::default();

    patterns
        .iter()
        .map(|pattern| number_of_combinations(&towels, pattern, &mut cache))
        .sum()
}

fn is_buildable(towels: &[&str], pattern: &str) -> bool {
    towels
        .iter()
        .filter_map(|towel| pattern.strip_prefix(towel))
        .any(|new_pat| new_pat.is_empty() || is_buildable(towels, new_pat))
}

fn number_of_combinations<'a>(
    towels: &[&str],
    pattern: &'a str,
    cache: &mut HashMap<&'a str, usize>,
) -> usize {
    if let Some(&cached) = cache.get(pattern) {
        return cached;
    }

    let out = towels
        .iter()
        .filter_map(|towel| pattern.strip_prefix(towel))
        .map(|new_pat| {
            if new_pat.is_empty() {
                1
            } else {
                number_of_combinations(towels, new_pat, cache)
            }
        })
        .sum();

    cache.insert(pattern, out);
    out
}

fn parse_input(input: &str) -> (Vec<&str>, Vec<&str>) {
    let (a, b) = input.split_once("\n\n").unwrap();
    let a = a.split(", ").collect();

    let b = b.lines().collect();

    (a, b)
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day19.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 6);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 263);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 16);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 723524534506343);
    }
}
