const INPUT: &str = include_str!("inputs/day04.txt");

pub fn run() -> (String, String) {
    (part_a(INPUT).to_string(), part_b(INPUT).to_string())
}

#[allow(clippy::collapsible_if)]
fn part_a(input: &str) -> usize {
    let base = parse_input(input);
    let mut out = 0_usize;

    for i in 0..base.len() {
        for j in 0..base[i].len() {
            if base[i][j] == 'X' {
                if base.get(i).and_then(|v| v.get(j + 1)) == Some(&'M')
                    && base.get(i).and_then(|v| v.get(j + 2)) == Some(&'A')
                    && base.get(i).and_then(|v| v.get(j + 3)) == Some(&'S')
                {
                    out += 1;
                }
                if base.get(i + 1).and_then(|v| v.get(j + 1)) == Some(&'M')
                    && base.get(i + 2).and_then(|v| v.get(j + 2)) == Some(&'A')
                    && base.get(i + 3).and_then(|v| v.get(j + 3)) == Some(&'S')
                {
                    out += 1;
                }
                if base.get(i + 1).and_then(|v| v.get(j)) == Some(&'M')
                    && base.get(i + 2).and_then(|v| v.get(j)) == Some(&'A')
                    && base.get(i + 3).and_then(|v| v.get(j)) == Some(&'S')
                {
                    out += 1;
                }
                if i >= 3 && j >= 3 {
                    if base.get(i - 1).and_then(|v| v.get(j - 1)) == Some(&'M')
                        && base.get(i - 2).and_then(|v| v.get(j - 2)) == Some(&'A')
                        && base.get(i - 3).and_then(|v| v.get(j - 3)) == Some(&'S')
                    {
                        out += 1;
                    }
                }
                if j >= 3 {
                    if base.get(i).and_then(|v| v.get(j - 1)) == Some(&'M')
                        && base.get(i).and_then(|v| v.get(j - 2)) == Some(&'A')
                        && base.get(i).and_then(|v| v.get(j - 3)) == Some(&'S')
                    {
                        out += 1;
                    }
                    if base.get(i + 1).and_then(|v| v.get(j - 1)) == Some(&'M')
                        && base.get(i + 2).and_then(|v| v.get(j - 2)) == Some(&'A')
                        && base.get(i + 3).and_then(|v| v.get(j - 3)) == Some(&'S')
                    {
                        out += 1;
                    }
                }
                if i >= 3 {
                    if base.get(i - 1).and_then(|v| v.get(j + 1)) == Some(&'M')
                        && base.get(i - 2).and_then(|v| v.get(j + 2)) == Some(&'A')
                        && base.get(i - 3).and_then(|v| v.get(j + 3)) == Some(&'S')
                    {
                        out += 1;
                    }
                    if base.get(i - 1).and_then(|v| v.get(j)) == Some(&'M')
                        && base.get(i - 2).and_then(|v| v.get(j)) == Some(&'A')
                        && base.get(i - 3).and_then(|v| v.get(j)) == Some(&'S')
                    {
                        out += 1;
                    }
                }
            }
        }
    }

    out
}

fn part_b(input: &str) -> usize {
    let base = parse_input(input);
    let mut out = 0_usize;

    for i in 1..(base.len() - 1) {
        for j in 1..(base[i].len() - 1) {
            if base[i][j] == 'A' {
                if base[i - 1][j - 1] == 'M'
                    && base[i - 1][j + 1] == 'M'
                    && base[i + 1][j - 1] == 'S'
                    && base[i + 1][j + 1] == 'S'
                {
                    out += 1;
                }
                if base[i - 1][j - 1] == 'M'
                    && base[i - 1][j + 1] == 'S'
                    && base[i + 1][j - 1] == 'M'
                    && base[i + 1][j + 1] == 'S'
                {
                    out += 1;
                }
                if base[i - 1][j - 1] == 'S'
                    && base[i - 1][j + 1] == 'S'
                    && base[i + 1][j - 1] == 'M'
                    && base[i + 1][j + 1] == 'M'
                {
                    out += 1;
                }
                if base[i - 1][j - 1] == 'S'
                    && base[i - 1][j + 1] == 'M'
                    && base[i + 1][j - 1] == 'S'
                    && base[i + 1][j + 1] == 'M'
                {
                    out += 1;
                }
            }
        }
    }

    out
}

fn parse_input(input: &str) -> Vec<Vec<char>> {
    input.lines().map(|line| line.chars().collect()).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day04.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 18);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 2378);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 9);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 1796);
    }
}
