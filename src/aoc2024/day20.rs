use crate::utils::{direction::NEIGHBOURS, position::Position, HashMap};
use grid::Grid;
use itertools::Itertools;
use rayon::prelude::*;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day20.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let (grid, start_pos, end_pos) = parse_input(input);
    cheat_step(&grid, start_pos, end_pos, 2, 100)
}

fn part_b(input: &str) -> usize {
    let (grid, start_pos, end_pos) = parse_input(input);
    cheat_step(&grid, start_pos, end_pos, 20, 100)
}

fn parse_input(input: &str) -> (Grid<bool>, Pos, Pos) {
    let mut start = None;
    let mut end = None;
    let t: Vec<Vec<bool>> = input
        .lines()
        .enumerate()
        .map(|(i, line)| {
            line.char_indices()
                .map(|(j, c)| match c {
                    'S' => {
                        start = Some(Pos::new(i, j));
                        false
                    }
                    'E' => {
                        end = Some(Pos::new(i, j));
                        false
                    }
                    '.' => false,
                    '#' => true,
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect();

    (Grid::from(t), start.unwrap(), end.unwrap())
}

fn cheat_step(
    grid: &Grid<bool>,
    start_pos: Pos,
    end_pos: Pos,
    cheat_distance: usize,
    saved_time: usize,
) -> usize {
    let mut path = HashMap::default();
    path.insert(start_pos, 0);

    let mut walker = start_pos;
    let mut i = 0_usize;

    while walker != end_pos {
        if let Some(new_p) = NEIGHBOURS
            .into_iter()
            .filter_map(|d| walker.try_step_in_grid(d, grid).filter(|p| !grid[p]))
            .find(|p| !path.contains_key(p))
        {
            i += 1;
            walker = new_p;
            path.insert(new_p, i);
        } else {
            unreachable!();
        }
    }
    path.insert(end_pos, i + 1);

    path.par_iter()
        .map(|(pos, i)| -> usize {
            poss_within_distance(grid, *pos, cheat_distance)
                .filter(|pot_p| path[pot_p] >= i + saved_time + pot_p.distance(pos))
                .count()
        })
        .sum::<usize>()
}

fn poss_within_distance(
    grid: &Grid<bool>,
    pos: Pos,
    cheat_distance: usize,
) -> impl Iterator<Item = Pos> {
    let dd = 0..=cheat_distance;

    let mut v: Vec<_> = NEIGHBOURS
        .into_iter()
        .flat_map(|d| {
            dd.clone()
                .cartesian_product(dd.clone())
                .filter(move |(dx, dy)| dx + dy <= cheat_distance)
                .filter_map(move |(dx, dy)| pos.try_steps(d, dx)?.try_steps(d.turn_right(), dy))
                .filter(|p| grid.get(p.0, p.1).is_some_and(|&b| !b))
        })
        .collect();

    v.sort_unstable();
    v.dedup();
    v.into_iter()
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day20.txt");

    #[test]
    fn test_a() {
        let (grid, start_pos, end_pos) = parse_input(TEST);
        assert_eq!(cheat_step(&grid, start_pos, end_pos, 2, 40), 2);
        assert_eq!(cheat_step(&grid, start_pos, end_pos, 2, 64), 1);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 1327);
    }

    #[test]
    fn test_b() {
        let (grid, start_pos, end_pos) = parse_input(TEST);
        assert_eq!(cheat_step(&grid, start_pos, end_pos, 20, 76), 3);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 985737);
    }
}
