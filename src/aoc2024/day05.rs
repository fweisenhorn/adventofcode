use crate::utils::HashSet;
use std::cmp::Ordering;

const INPUT: &str = include_str!("inputs/day05.txt");

pub fn run() -> (u32, u32) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u32 {
    let (rules, pages) = parse_input(input);

    pages
        .iter()
        .filter(|page| page.is_sorted_by(|&x, &y| compare(x, y, &rules) != Ordering::Greater))
        .map(|page| page[(page.len() - 1) / 2])
        .sum()
}

fn part_b(input: &str) -> u32 {
    let (rules, pages) = parse_input(input);

    pages
        .into_iter()
        .filter(|page| !page.is_sorted_by(|&x, &y| compare(x, y, &rules) != Ordering::Greater))
        .map(|mut page| {
            page.sort_unstable_by(|&x, &y| compare(x, y, &rules));
            page[(page.len() - 1) / 2]
        })
        .sum()
}

fn parse_input(input: &str) -> (HashSet<(u32, u32)>, Vec<Vec<u32>>) {
    let (a, b) = input.split_once("\n\n").unwrap();

    let a = a
        .lines()
        .map(|line| {
            let (x, y) = line.split_once('|').unwrap();
            (x.parse().unwrap(), y.parse().unwrap())
        })
        .collect();

    let b = b
        .lines()
        .map(|line| line.split(',').map(|n| n.parse().unwrap()).collect())
        .collect();

    (a, b)
}

fn compare(x: u32, y: u32, rules: &HashSet<(u32, u32)>) -> Ordering {
    if x == y {
        Ordering::Equal
    } else if rules.contains(&(x, y)) {
        Ordering::Less
    } else if rules.contains(&(y, x)) {
        Ordering::Greater
    } else {
        unreachable!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day05.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 143);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 5108);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 123);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 7380);
    }
}
