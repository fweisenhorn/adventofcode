use crate::utils::{position::Position, HashMap, HashSet};

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day08.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let mut pos_set = HashSet::default();
    let all_antenna = parse_input(input);

    let bounds = Pos::new(
        input.lines().count(),
        input.lines().next().unwrap().chars().count(),
    );

    for antenna_vec in &all_antenna {
        for an1 in antenna_vec {
            for an2 in antenna_vec {
                if an1 == an2 {
                    continue;
                }

                if let Some(p) = add_sub_max_pos(an1, an1, an2, bounds) {
                    pos_set.insert(p);
                }
            }
        }
    }

    pos_set.len()
}

fn part_b(input: &str) -> usize {
    let mut pos_set = HashSet::default();
    let all_antenna = parse_input(input);

    let bounds = Pos::new(
        input.lines().count(),
        input.lines().next().unwrap().chars().count(),
    );

    for antenna_vec in &all_antenna {
        for an1 in antenna_vec {
            for an2 in antenna_vec {
                if an1 == an2 {
                    continue;
                }

                let mut t = Some(*an1);

                while let Some(p) = t {
                    pos_set.insert(p);
                    t = add_sub_max_pos(&p, an1, an2, bounds);
                }
            }
        }
    }

    pos_set.len()
}

fn parse_input(input: &str) -> Vec<Vec<Pos>> {
    let mut antennas: HashMap<char, Vec<Pos>> = HashMap::default();

    for (x, line) in input.lines().enumerate() {
        for (y, c) in line.char_indices() {
            if c != '.' {
                antennas.entry(c).or_default().push(Pos::new(x, y));
            }
        }
    }

    antennas.into_values().collect()
}

fn add_sub_max_pos(or: &Pos, to: &Pos, from: &Pos, bounds: Pos) -> Option<Pos> {
    let f = |a: usize, b: usize, c: usize, d: usize| -> Option<usize> {
        a.checked_add(b)
            .and_then(|x| x.checked_sub(c))
            .filter(|x| *x < d)
    };

    let x = f(or.0, to.0, from.0, bounds.0)?;
    let y = f(or.1, to.1, from.1, bounds.1)?;

    Some(Pos::new(x, y))
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day08.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 14);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 376);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 34);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 1352);
    }
}
