use crate::utils::HashMap;
use std::collections::VecDeque;

const INPUT: &str = include_str!("inputs/day24.txt");

pub fn run() -> (usize, String) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let (mut wires, mut gates) = parse_input(input);

    while let Some(gate) = gates.pop_front() {
        if !(wires.contains_key(&gate.in1) && wires.contains_key(&gate.in2)) {
            gates.push_back(gate);
            continue;
        }

        assert!(!wires.contains_key(&gate.out));

        wires.insert(
            gate.out,
            match gate.op {
                Op::And => wires[&gate.in1] & wires[&gate.in2],
                Op::Or => wires[&gate.in1] | wires[&gate.in2],
                Op::Xor => wires[&gate.in1] ^ wires[&gate.in2],
            },
        );
    }

    value_from_prefix(&wires, "z")
}

fn part_b(input: &str) -> String {
    let (_, gates) = parse_input(input);

    let f = |gate: &Gate| -> bool {
        let xyz = ['x', 'y', 'z'];
        !(gate.in1.starts_with(xyz) || gate.in2.starts_with(xyz) || gate.out.starts_with(xyz))
    };

    let g = |gate: &Gate| -> bool { gate.in1 != "x00" && gate.in2 != "x00" };

    let h = |gate: &Gate, y: Op| -> bool {
        gates
            .iter()
            .any(|g| y == g.op && (gate.out == g.in1 || gate.out == g.in2))
    };

    let mut v: Vec<&str> = gates
        .iter()
        .filter(|gate| {
            (gate.op == Op::Xor && f(gate))
                || (gate.op == Op::And && g(gate) && h(gate, Op::Xor))
                || (gate.op == Op::Xor && g(gate) && h(gate, Op::Or))
                || (gate.op != Op::Xor && gate.out.starts_with('z') && gate.out != "z45")
        })
        .map(|gate| gate.out)
        .collect();
    v.sort_unstable();
    v.join(",")
}

#[derive(PartialEq, Eq)]
enum Op {
    And,
    Or,
    Xor,
}

struct Gate<'a> {
    in1: &'a str,
    in2: &'a str,
    op: Op,
    out: &'a str,
}

fn parse_input(input: &str) -> (HashMap<&str, bool>, VecDeque<Gate>) {
    let (a, b) = input.split_once("\n\n").unwrap();

    let a = a
        .lines()
        .map(|line| {
            let (t1, t2) = line.split_once(": ").unwrap();
            let t2 = match t2 {
                "0" => false,
                "1" => true,
                _ => unreachable!(),
            };
            (t1, t2)
        })
        .collect();

    let b = b
        .lines()
        .map(|line| {
            let mut t = line.split_whitespace();

            let t1 = t.next().unwrap();
            let t2 = match t.next().unwrap() {
                "AND" => Op::And,
                "OR" => Op::Or,
                "XOR" => Op::Xor,
                _ => unreachable!(),
            };
            let t3 = t.next().unwrap();
            t.next();
            let t4 = t.next().unwrap();
            assert!(t.next().is_none());

            Gate {
                in1: t1,
                in2: t3,
                op: t2,
                out: t4,
            }
        })
        .collect();

    (a, b)
}

fn value_from_prefix(wires: &HashMap<&str, bool>, prefix: &str) -> usize {
    let mut z_wires: Vec<_> = wires
        .iter()
        .filter(|(n, _)| n.starts_with(prefix))
        .collect();

    z_wires.sort_unstable();

    z_wires.into_iter().rev().fold(0, |mut acc, (_, &b)| {
        acc <<= 1;
        if b {
            acc += 1;
        }
        acc
    })
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day24.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 2024);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 56729630917616);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "bjm,hsw,nvr,skf,wkr,z07,z13,z18");
    }
}
