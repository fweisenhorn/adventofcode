use crate::utils::{direction::NEIGHBOURS, position::Position, HashSet};
use grid::Grid;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day10.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let area = parse_input(input);

    area.indexed_iter()
        .filter(|(_, &c)| c == b'0')
        .map(|((i, j), _)| {
            let mut peaks = HashSet::default();
            route_search(
                Pos::new(i, j),
                &area,
                &mut peaks,
                &mut HashSet::default(),
                vec![],
            );
            peaks.len()
        })
        .sum()
}

fn part_b(input: &str) -> usize {
    let area = parse_input(input);

    area.indexed_iter()
        .filter(|(_, &c)| c == b'0')
        .map(|((i, j), _)| {
            let mut routes = HashSet::default();
            route_search(
                Pos::new(i, j),
                &area,
                &mut HashSet::default(),
                &mut routes,
                vec![],
            );
            routes.len()
        })
        .sum()
}

fn route_search(
    pos: Pos,
    area: &Grid<u8>,
    peaks: &mut HashSet<Pos>,
    routes: &mut HashSet<Vec<Pos>>,
    mut route: Vec<Pos>,
) {
    route.push(pos);

    if area[pos] == b'9' {
        peaks.insert(pos);
        routes.insert(route.clone());
        return;
    }

    NEIGHBOURS
        .into_iter()
        .filter_map(|d| pos.try_step_in_grid(d, area))
        .filter(|&new_pos| area[new_pos] == area[pos] + 1)
        .for_each(|new_pos| route_search(new_pos, area, peaks, routes, route.clone()));
}

fn parse_input(input: &str) -> Grid<u8> {
    let t: Vec<Vec<u8>> = input.lines().map(|line| line.as_bytes().to_vec()).collect();
    Grid::from(t)
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day10.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 36);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 552);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 81);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 1225);
    }
}
