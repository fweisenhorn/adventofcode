const INPUT: &str = include_str!("inputs/day03.txt");

pub fn run() -> (u32, u32) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u32 {
    let t: Vec<char> = input.trim().chars().collect();
    let mut out = 0;

    let mut i = 0;

    while i < t.len() {
        scan_for_muls(&t, &mut i, &mut out);
        i += 1;
    }

    out
}

fn part_b(input: &str) -> u32 {
    let t: Vec<char> = input.trim().chars().collect();
    let mut out = 0;

    let mut i = 0;
    let mut do_or_dont = true;

    while i < t.len() {
        if do_or_dont {
            if t.get(i..(i + 7)) == Some(&['d', 'o', 'n', '\'', 't', '(', ')']) {
                do_or_dont = false;
                i += 1;
                continue;
            }
            scan_for_muls(&t, &mut i, &mut out);
        } else if t.get(i..(i + 4)) == Some(&['d', 'o', '(', ')']) {
            do_or_dont = true;
        }

        i += 1;
    }

    out
}

fn scan_for_muls(t: &[char], i: &mut usize, out: &mut u32) {
    if t.get(*i..(*i + 4)) == Some(&['m', 'u', 'l', '(']) {
        *i += 4;
        if let Some(x) = parse_number(t, i) {
            if t.get(*i) == Some(&',') {
                *i += 1;
                if let Some(y) = parse_number(t, i) {
                    if t.get(*i) == Some(&')') {
                        *out += x * y;
                    }
                }
            }
        }
    }
}

fn parse_number(t: &[char], i: &mut usize) -> Option<u32> {
    let mut n = 0;

    if t.get(*i)?.is_numeric() {
        n = n * 10 + t.get(*i).unwrap().to_digit(10).unwrap();
        *i += 1;
        if t.get(*i)?.is_numeric() {
            n = n * 10 + t.get(*i).unwrap().to_digit(10).unwrap();
            *i += 1;
            if t.get(*i)?.is_numeric() {
                n = n * 10 + t.get(*i).unwrap().to_digit(10).unwrap();
                *i += 1;
            }
        }
        Some(n)
    } else {
        None
    }
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day03.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 161);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 182780583);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 48);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 90772405);
    }
}
