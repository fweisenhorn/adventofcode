use crate::utils::concat_ints;
use rayon::prelude::*;

const INPUT: &str = include_str!("inputs/day07.txt");

pub fn run() -> (u64, u64) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u64 {
    parse_input(input)
        .par_iter()
        .filter(|(a, v)| check_logic(0, v, *a, false))
        .map(|(a, _)| a)
        .sum()
}

fn part_b(input: &str) -> u64 {
    parse_input(input)
        .par_iter()
        .filter(|(a, v)| check_logic(0, v, *a, true))
        .map(|(a, _)| a)
        .sum()
}

fn parse_input(input: &str) -> Vec<(u64, Vec<u64>)> {
    input
        .lines()
        .map(|line| {
            let (a, b) = line.split_once(": ").unwrap();
            let a = a.parse().unwrap();
            let b = b.split_whitespace().map(|x| x.parse().unwrap()).collect();
            (a, b)
        })
        .collect()
}

fn check_logic(running: u64, remaining: &[u64], target: u64, part_b: bool) -> bool {
    if remaining.is_empty() {
        return target == running;
    }
    if running > target {
        return false;
    }

    check_logic(running + remaining[0], &remaining[1..], target, part_b)
        || check_logic(running * remaining[0], &remaining[1..], target, part_b)
        || (part_b
            && check_logic(
                concat_ints(running, remaining[0]),
                &remaining[1..],
                target,
                part_b,
            ))
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day07.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 3749);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 2664460013123);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 11387);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 426214131924213);
    }
}
