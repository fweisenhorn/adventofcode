const INPUT: &str = include_str!("inputs/day14.txt");

pub fn run() -> (u32, u32) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u32 {
    let area = if input == INPUT { [101, 103] } else { [11, 7] };
    let mut robots = parse_input(input, area);

    for _ in 0..100 {
        for robot in &mut robots {
            robot.step(area);
        }
    }

    safety_factor(&robots, area)
}

fn part_b(input: &str) -> u32 {
    let area = if input == INPUT { [101, 103] } else { [11, 7] };

    let mut robots = parse_input(input, area);

    // manually find the times with the lowest safety score:

    let mut scores = Vec::with_capacity(101 * 103);
    scores.push((0, safety_factor(&robots, area)));

    for i in 0..101 * 103 {
        for robot in &mut robots {
            robot.step(area);
        }
        scores.push((i + 1, safety_factor(&robots, area)));
    }

    scores.sort_by_key(|x| x.1);
    // assert_eq!(&scores[0..5], &[]);
    // [(7412, 32760000), (645, 46698960), (4079, 50423856), (9331, 50638770), (1655, 51351552), ...

    // for robot in &mut robots {
    //     robot.steps(area, 7412);
    // }
    // let mut grid = vec![vec![' '; area[1] as usize]; area[0] as usize];
    // for robot in robots {
    //     grid[robot.p[0] as usize][robot.p[1] as usize] = '#';
    // }
    // for v in grid {
    //     for c in v {
    //         print!("{c}");
    //     }
    //     println!();
    // }

    scores[0].0
}

fn parse_input(input: &str, area: [u8; 2]) -> Vec<Robot> {
    input
        .lines()
        .map(|line| {
            let (p, v) = line.split_once(" v=").unwrap();

            let p = p[2..p.len()].split_once(',').unwrap();
            let p0 = p.0.parse().unwrap();
            let p1 = p.1.parse().unwrap();

            let v = v.split_once(',').unwrap();
            let v0 = v.0.parse::<i8>().unwrap();
            let v1 = v.1.parse::<i8>().unwrap();

            let v0 = area[0].checked_add_signed(v0).unwrap() % area[0];
            let v1 = area[1].checked_add_signed(v1).unwrap() % area[1];

            Robot {
                p: [p0, p1],
                v: [v0, v1],
            }
        })
        .collect()
}

fn safety_factor(robots: &[Robot], area: [u8; 2]) -> u32 {
    let mid = [area[0] / 2, area[1] / 2];

    let mut out = [0; 4];

    for robot in robots {
        if robot.p[0] < mid[0] && robot.p[1] < mid[1] {
            out[0] += 1;
        } else if robot.p[0] < mid[0] && robot.p[1] > mid[1] {
            out[1] += 1;
        } else if robot.p[0] > mid[0] && robot.p[1] < mid[1] {
            out[2] += 1;
        } else if robot.p[0] > mid[0] && robot.p[1] > mid[1] {
            out[3] += 1;
        }
    }

    out.iter().product()
}

#[derive(PartialOrd, Ord, PartialEq, Eq, Hash, Copy, Clone, Default, Debug)]
struct Robot {
    p: [u8; 2],
    v: [u8; 2],
}

impl Robot {
    fn step(&mut self, area: [u8; 2]) {
        self.p[0] = (self.p[0] + self.v[0]) % area[0];
        self.p[1] = (self.p[1] + self.v[1]) % area[1];
    }
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day14.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 12);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 217328832);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 7412);
    }
}
