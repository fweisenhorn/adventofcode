use crate::utils::{direction::NEIGHBOURS, position::Position, HashSet};
use grid::Grid;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day12.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let grid = parse_input(input);
    find_all_regions(&grid)
        .iter()
        .map(|r| r.len() * perimeter_length(r, &grid))
        .sum()
}

fn part_b(input: &str) -> usize {
    let grid = parse_input(input);
    find_all_regions(&grid)
        .iter()
        .map(|r| r.len() * sides(r, &grid))
        .sum()
}

fn parse_input(input: &str) -> Grid<u8> {
    let x: Vec<Vec<u8>> = input.lines().map(|line| line.as_bytes().to_vec()).collect();
    Grid::from(x)
}

fn find_all_regions(grid: &Grid<u8>) -> Vec<HashSet<Pos>> {
    let mut regions: Vec<HashSet<Pos>> = vec![];

    for ((i, j), _) in grid.indexed_iter() {
        let pos = Pos::new(i, j);

        if regions.iter().any(|v| v.contains(&pos)) {
            continue;
        }

        regions.push(find_region(pos, grid));
    }

    regions
}

fn find_region(pos: Pos, grid: &Grid<u8>) -> HashSet<Pos> {
    let mut curr_region = HashSet::default();
    let mut queue = vec![pos];

    while let Some(new_pos) = queue.pop() {
        if curr_region.contains(&new_pos) {
            continue;
        }

        curr_region.insert(new_pos);

        NEIGHBOURS
            .into_iter()
            .filter_map(|d| new_pos.try_step_in_grid(d, grid))
            .filter(|next_pos| grid[*next_pos] == grid[pos])
            .for_each(|next_pos| queue.push(next_pos));
    }

    curr_region
}

fn perimeter_length(region: &HashSet<Pos>, grid: &Grid<u8>) -> usize {
    region
        .iter()
        .map(|p| {
            NEIGHBOURS
                .into_iter()
                .map(|d| p.try_step_in_grid(d, grid))
                .filter(|x| x.is_none_or(|t| !region.contains(&t)))
                .count()
        })
        .sum()
}

fn sides(region: &HashSet<Pos>, grid: &Grid<u8>) -> usize {
    region
        .iter()
        .map(|p| {
            NEIGHBOURS
                .into_iter()
                .filter(|&d| {
                    let p1 = p.try_step_in_grid(d, grid);
                    let p2 = p.try_step_in_grid(d.turn_left(), grid);
                    let p3 = p2.and_then(|t| t.try_step_in_grid(d, grid));
                    p1.is_none_or(|t| !region.contains(&t))
                        && !(p2.is_some_and(|t| region.contains(&t))
                            && p3.is_none_or(|t| !region.contains(&t)))
                })
                .count()
        })
        .sum()
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST_A: &str = "\
            AAAA\n\
            BBCD\n\
            BBCC\n\
            EEEC";
    const TEST_B: &str = include_str!("tests/day12.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST_A), 140);
        assert_eq!(part_a(TEST_B), 1930);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 1421958);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST_A), 80);
        assert_eq!(part_b(TEST_B), 1206);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 885394);
    }
}
