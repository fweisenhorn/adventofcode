const INPUT: &str = include_str!("inputs/day13.txt");

pub fn run() -> (u64, u64) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u64 {
    parse_input(input)
        .into_iter()
        .filter_map(find_multiples)
        .map(|x| 3 * x[0] + x[1])
        .sum()
}

fn part_b(input: &str) -> u64 {
    parse_input(input)
        .iter()
        .map(change_input_for_part_b)
        .filter_map(find_multiples)
        .map(|x| 3 * x[0] + x[1])
        .sum()
}

fn parse_input(input: &str) -> Vec<[u64; 6]> {
    input
        .split("\n\n")
        .map(|segment| {
            let mut lines = segment.lines();

            let line = lines.next().unwrap();
            let (_, a) = line.split_once("X+").unwrap();
            let (x1, a) = a.split_once(", ").unwrap();
            let (_, y1) = a.split_once("Y+").unwrap();

            let line = lines.next().unwrap();
            let (_, a) = line.split_once("X+").unwrap();
            let (x2, a) = a.split_once(", ").unwrap();
            let (_, y2) = a.split_once("Y+").unwrap();

            let line = lines.next().unwrap();
            let (_, a) = line.split_once("X=").unwrap();
            let (x3, a) = a.split_once(", ").unwrap();
            let (_, y3) = a.split_once("Y=").unwrap();

            assert_eq!(lines.count(), 0);

            [
                x1.parse().unwrap(),
                x2.parse().unwrap(),
                y1.parse().unwrap(),
                y2.parse().unwrap(),
                x3.parse().unwrap(),
                y3.parse().unwrap(),
            ]
        })
        .collect()
}

const fn change_input_for_part_b(x: &[u64; 6]) -> [u64; 6] {
    [
        x[0],
        x[1],
        x[2],
        x[3],
        x[4] + 10_000_000_000_000,
        x[5] + 10_000_000_000_000,
    ]
}

const fn find_multiples(x: [u64; 6]) -> Option<[u64; 2]> {
    let n1 = x[3] * x[4];
    let n2 = x[1] * x[5];
    let d1 = x[3] * x[0];
    let d2 = x[1] * x[2];

    let (numer, denom) = if n1 > n2 && d1 > d2 {
        (n1 - n2, d1 - d2)
    } else if n1 < n2 && d1 < d2 {
        (n2 - n1, d2 - d1)
    } else if n1 == n2 && d1 != d2 {
        (0, 1)
    } else {
        return None;
    };

    if numer % denom != 0 {
        return None;
    }
    let x1 = numer / denom;

    if x[2] * x1 >= x[5] {
        return None;
    }
    let numer2 = x[5] - x[2] * x1;

    if numer2 % x[3] != 0 {
        return None;
    }
    let x2 = numer2 / x[3];

    Some([x1, x2])
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day13.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 480);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 28262);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 101406661266314);
    }
}
