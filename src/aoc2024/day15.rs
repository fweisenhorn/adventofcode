use crate::utils::{direction::Direction, position::Position};
use std::collections::VecDeque;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day15.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let (mut grid, dirs) = parse_input(input);

    'outer: for d in dirs {
        let mut moved_boxes = vec![];
        let mut queue: VecDeque<_> = vec![robot(&grid)].into();

        'inner: while let Some(mut t) = queue.pop_front() {
            if grid[t.0][t.1] == '#' {
                continue 'outer;
            }
            if grid[t.0][t.1] == '.' {
                continue 'inner;
            }
            t = t.steps_unchecked(d, 1);
            moved_boxes.push(t);
            queue.push_back(t);
        }

        move_boxes(moved_boxes, d, &mut grid);
    }

    sum_result(grid)
}

fn part_b(input: &str) -> usize {
    let (mut grid, dirs) = parse_input(input);

    grid = widen_grid_for_part_b(&grid);

    'outer: for d in dirs {
        let mut moved_boxes = vec![];
        let mut queue: VecDeque<_> = vec![robot(&grid)].into();

        'inner: while let Some(mut t) = queue.pop_front() {
            if grid[t.0][t.1] == '#' {
                continue 'outer;
            }
            if grid[t.0][t.1] == '.' {
                continue 'inner;
            }

            t = t.steps_unchecked(d, 1);
            if moved_boxes.contains(&t) {
                continue 'inner;
            }
            moved_boxes.push(t);
            queue.push_back(t);
            if d.is_vertical() {
                if grid[t.0][t.1] == '[' {
                    queue.push_back(t.steps_unchecked(Direction::Right, 1));
                }
                if grid[t.0][t.1] == ']' {
                    queue.push_back(t.steps_unchecked(Direction::Left, 1));
                }
            }
        }

        move_boxes(moved_boxes, d, &mut grid);
    }

    sum_result(grid)
}

fn parse_input(input: &str) -> (Vec<Vec<char>>, Vec<Direction>) {
    let (a, b) = input.split_once("\n\n").unwrap();

    let grid = a.lines().map(|line| line.chars().collect()).collect();

    let dirs: Vec<Direction> = b
        .lines()
        .flat_map(|line| line.bytes().map(|b| Direction::try_from(b).unwrap()))
        .collect();

    (grid, dirs)
}

fn widen_grid_for_part_b(grid: &[Vec<char>]) -> Vec<Vec<char>> {
    let mut out = Vec::with_capacity(grid.len());

    for (i, v) in grid.iter().enumerate() {
        out.push(Vec::with_capacity(2 * v.len()));

        for c in v {
            match c {
                '.' => {
                    out[i].push('.');
                    out[i].push('.');
                }
                '#' => {
                    out[i].push('#');
                    out[i].push('#');
                }
                'O' => {
                    out[i].push('[');
                    out[i].push(']');
                }
                '@' => {
                    out[i].push('@');
                    out[i].push('.');
                }

                _ => unreachable!(),
            }
        }
    }
    out
}

fn robot(grid: &[Vec<char>]) -> Pos {
    let x = grid.iter().position(|v| v.contains(&'@')).unwrap();
    Pos::new(x, grid[x].iter().position(|c| *c == '@').unwrap())
}

fn move_boxes(moved_boxes: Vec<Position<usize>>, d: Direction, grid: &mut [Vec<char>]) {
    for s in moved_boxes.into_iter().rev() {
        let s1 = s.steps_unchecked(d.turn_around(), 1);
        (grid[s.0][s.1], grid[s1.0][s1.1]) = (grid[s1.0][s1.1], grid[s.0][s.1]);
    }
}

fn sum_result(grid: Vec<Vec<char>>) -> usize {
    grid.into_iter()
        .enumerate()
        .map(|(i, v)| -> usize {
            v.into_iter()
                .enumerate()
                .filter(|(_, c)| matches!(c, 'O' | '['))
                .map(|(j, _)| 100 * i + j)
                .sum()
        })
        .sum()
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day15.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 10092);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 1456590);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 9021);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 1489116);
    }
}
