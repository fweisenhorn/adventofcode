use crate::utils::{
    direction::{Direction, NEIGHBOURS},
    position::Position,
    HashMap,
};
use grid::Grid;
use std::cmp::Reverse;
use std::collections::BinaryHeap;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day18.txt");

pub fn run() -> (usize, String) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let bytes = parse_input(input);

    let n_bytes = if input == INPUT { 1024 } else { 12 };
    let grid_size = if input == INPUT { 70 } else { 6 };

    let mut grid = Grid::init(grid_size + 1, grid_size + 1, false);

    for byte in &bytes[0..n_bytes] {
        grid[byte] = true;
    }

    let end_pos = Pos::new(grid_size, grid_size);

    let mut cache = HashMap::default();
    let mut queue = BinaryHeap::new();
    queue.push((Reverse(0), Pos::new(0, 0)));

    while let Some((Reverse(n), p)) = queue.pop() {
        if p == end_pos {
            return n;
        }
        if cache.get(&p).is_some_and(|i| *i <= n) {
            continue;
        }
        cache.insert(p, n);

        NEIGHBOURS
            .into_iter()
            .filter_map(|d| p.try_step_in_grid(d, &grid))
            .filter(|new_p| !grid[new_p])
            .for_each(|new_p| queue.push((Reverse(n + 1), new_p)));
    }

    unreachable!()
}

fn part_b(input: &str) -> String {
    let bytes = parse_input(input);

    let grid_size = if input == INPUT { 70 } else { 6 };

    let mut grid = Grid::init(grid_size + 1, grid_size + 1, false);

    let mut queue = BinaryHeap::new();
    let mut cache = HashMap::default();

    for byte in &bytes {
        grid[byte] = true;
        if byte.0 == grid_size || byte.1 == 0 {
            queue.push((Reverse(0), *byte));
        }

        ortho_and_diagonal_neighbours(byte, &grid)
            .filter_map(|p| cache.get(&p))
            .for_each(|n| queue.push((Reverse(n + 1), *byte)));

        while let Some((Reverse(n), p)) = queue.pop() {
            if p.0 == 0 || p.1 == grid_size {
                return byte.1.to_string() + "," + &byte.0.to_string();
            }

            if cache.get(&p).is_some_and(|i| *i <= n) {
                continue;
            }
            cache.insert(p, n);

            ortho_and_diagonal_neighbours(&p, &grid)
                .filter(|new_p| grid[new_p] && cache.get(new_p).is_none_or(|i| *i < n - 1))
                .for_each(|new_p| {
                    queue.push((Reverse(n + 1), new_p));
                });
        }
    }

    unreachable!()
}

fn ortho_and_diagonal_neighbours(p: &Pos, grid: &Grid<bool>) -> impl Iterator<Item = Pos> {
    [
        p.try_step_in_grid(Direction::Up, grid)
            .and_then(|p| p.try_step_in_grid(Direction::Left, grid)),
        p.try_step_in_grid(Direction::Up, grid),
        p.try_step_in_grid(Direction::Up, grid)
            .and_then(|p| p.try_step_in_grid(Direction::Right, grid)),
        p.try_step_in_grid(Direction::Down, grid)
            .and_then(|p| p.try_step_in_grid(Direction::Left, grid)),
        p.try_step_in_grid(Direction::Down, grid),
        p.try_step_in_grid(Direction::Down, grid)
            .and_then(|p| p.try_step_in_grid(Direction::Right, grid)),
        p.try_step_in_grid(Direction::Left, grid),
        p.try_step_in_grid(Direction::Right, grid),
    ]
    .into_iter()
    .flatten()
}

fn parse_input(input: &str) -> Vec<Pos> {
    input
        .lines()
        .map(|line| {
            let (a, b) = line.split_once(',').unwrap();
            Pos::new(b.parse().unwrap(), a.parse().unwrap())
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day18.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 22);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 246);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "6,1");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "22,50");
    }
}
