use crate::utils::{
    direction::{Direction, NEIGHBOURS},
    position::Position,
    HashMap, HashSet,
};
use grid::Grid;
use std::{
    cmp::{min, Reverse},
    collections::BinaryHeap,
};

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day16.txt");

pub fn run() -> (u32, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u32 {
    let (grid, start_pos, end_pos) = parse_input(input);
    walk_forward(&grid, start_pos, end_pos, &mut HashMap::default())
}

fn part_b(input: &str) -> usize {
    let (grid, start_pos, end_pos) = parse_input(input);

    let mut cache = HashMap::default();
    let part_a_result = walk_forward(&grid, start_pos, end_pos, &mut cache);

    let mut paths: HashSet<Pos> = HashSet::default();
    let mut queue: BinaryHeap<(u32, Pos, Direction)> = BinaryHeap::new();

    for d in NEIGHBOURS {
        queue.push((part_a_result, end_pos, d));
    }
    paths.insert(end_pos);

    while let Some((score, curr_p, curr_d)) = queue.pop() {
        NEIGHBOURS
            .into_iter()
            .filter_map(|last_d| {
                let last_p = curr_p
                    .try_step_in_grid(curr_d.turn_around(), &grid)
                    .filter(|p| !grid[p])?;

                let last_score = score.checked_sub(if last_d == curr_d { 1 } else { 1001 })?;

                if cache
                    .get(&(last_p, last_d))
                    .is_some_and(|&cached| cached == last_score)
                {
                    Some((last_score, last_p, last_d))
                } else {
                    None
                }
            })
            .for_each(|t| {
                queue.push(t);
                paths.insert(t.1);
            });
    }

    paths.len()
}

fn walk_forward(
    grid: &Grid<bool>,
    start_pos: Pos,
    end_pos: Pos,
    cache: &mut HashMap<(Pos, Direction), u32>,
) -> u32 {
    let mut queue: BinaryHeap<(Reverse<u32>, Pos, Direction)> = BinaryHeap::new();

    queue.push((Reverse(0), start_pos, Direction::Right));
    cache.insert((start_pos, Direction::Right), 0);

    let mut out = u32::MAX;

    while let Some((Reverse(score), curr_p, curr_d)) = queue.pop() {
        if score > out {
            return out;
        }

        if curr_p == end_pos {
            out = min(out, score);
        }

        NEIGHBOURS
            .into_iter()
            .filter_map(|new_d| {
                let new_p = curr_p.try_step_in_grid(new_d, grid).filter(|p| !grid[p])?;
                let new_score = score + if new_d == curr_d { 1 } else { 1001 };

                Some((new_score, new_p, new_d))
            })
            .for_each(|(new_score, new_p, new_d)| {
                if cache
                    .get(&(new_p, new_d))
                    .is_some_and(|&cached| cached <= new_score)
                {
                    return;
                }

                cache.insert((new_p, new_d), new_score);
                queue.push((Reverse(new_score), new_p, new_d));
            });
    }

    unreachable!()
}

fn parse_input(input: &str) -> (Grid<bool>, Pos, Pos) {
    let mut start = None;
    let mut end = None;
    let t: Vec<Vec<bool>> = input
        .lines()
        .enumerate()
        .map(|(i, line)| {
            line.char_indices()
                .map(|(j, c)| match c {
                    '.' => false,
                    '#' => true,
                    'S' => {
                        start = Some(Pos::new(i, j));
                        false
                    }
                    'E' => {
                        end = Some(Pos::new(i, j));
                        false
                    }
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect();

    (Grid::from(t), start.unwrap(), end.unwrap())
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day16.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 7036);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 95444);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 45);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 513);
    }
}
