const INPUT: &str = include_str!("inputs/day17.txt");

pub fn run() {
    println!("{}", part_a(INPUT));
    println!("{}", part_b(INPUT));
}

// TODO
fn part_a(_input: &str) -> String {
    todo!()
}

fn part_b(_input: &str) -> String {
    todo!()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day17.txt");

    #[test]
    #[ignore]
    fn test_a() {
        assert_eq!(part_a(TEST), "");
    }

    #[test]
    #[ignore]
    fn result_a() {
        assert_eq!(part_a(INPUT), "");
    }

    #[test]
    #[ignore]
    fn test_b() {
        assert_eq!(part_b(TEST), "");
    }

    #[test]
    #[ignore]
    fn result_b() {
        assert_eq!(part_b(INPUT), "");
    }
}
