const INPUT: &str = include_str!("inputs/day02.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

pub fn part_a(input: &str) -> usize {
    parse_input(input).filter(|x| part_a_filter(x)).count()
}

fn part_b(input: &str) -> usize {
    parse_input(input).filter(|x| part_b_filter(x)).count()
}

fn parse_input(input: &str) -> impl Iterator<Item = Vec<i32>> + '_ {
    input.lines().map(|line| {
        line.split_whitespace()
            .map(|x| x.parse().unwrap())
            .collect()
    })
}

#[allow(clippy::manual_range_contains)]
fn part_a_filter(line: &[i32]) -> bool {
    (line.iter().is_sorted() || line.iter().rev().is_sorted())
        && line
            .windows(2)
            .map(|x| x[0].abs_diff(x[1]))
            .all(|x| 1 <= x && x <= 3)
}

fn part_b_filter(line: &[i32]) -> bool {
    part_a_filter(line) || {
        (0..line.len()).any(|i| {
            let mut v = line.to_owned();
            v.remove(i);
            part_a_filter(&v)
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day02.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 2);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 472);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 4);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 520);
    }
}
