use crate::utils::{iter_freq_counts, HashMap};

const INPUT: &str = include_str!("inputs/day11.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let mut nums = iter_freq_counts(input.split_whitespace().map(|x| x.parse::<u64>().unwrap()));

    for _ in 0..25 {
        nums = apply_rules_to_map(&nums);
    }

    nums.values().sum()
}

fn part_b(input: &str) -> usize {
    let mut nums = iter_freq_counts(input.split_whitespace().map(|x| x.parse::<u64>().unwrap()));

    for _ in 0..75 {
        nums = apply_rules_to_map(&nums);
    }

    nums.values().sum()
}

fn apply_rules_to_map(nums: &HashMap<u64, usize>) -> HashMap<u64, usize> {
    let mut out = HashMap::default();

    for (&num, freq) in nums {
        if num == 0 {
            *out.entry(1).or_insert(0) += freq;
        } else if num.ilog10() % 2 != 0 {
            let n = num.ilog10();
            *out.entry(num / 10_u64.pow((n + 1) / 2)).or_insert(0) += freq;
            *out.entry(num % 10_u64.pow((n + 1) / 2)).or_insert(0) += freq;
        } else {
            *out.entry(num * 2024).or_insert(0) += freq;
        }
    }

    out
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day11.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 55312);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 228668);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 270673834779359);
    }
}
