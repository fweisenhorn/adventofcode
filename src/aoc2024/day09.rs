const INPUT: &str = include_str!("inputs/day09.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let disk: Vec<Option<usize>> = input
        .trim()
        .char_indices()
        .flat_map(|(i, c)| {
            vec![
                if i % 2 == 0 { Some(i / 2) } else { None };
                c.to_digit(10).expect("Input should parse.") as usize
            ]
        })
        .collect();

    let mut out = 0;
    let mut i = 0;
    let mut j = disk.len() - 1;

    while i <= j {
        if let Some(x) = disk[i] {
            out += i * x;
            i += 1;
        } else {
            if let Some(x) = disk[j] {
                out += i * x;
                i += 1;
            }
            j -= 1;
        }
    }

    out
}

// TODO: Optimize ?
// Idea: Keep only list of Some-values with starting position?
fn part_b(input: &str) -> usize {
    let mut positions = Vec::with_capacity(input.len() / 2);

    let mut rep: Vec<File> = input
        .trim()
        .char_indices()
        .map(|(i, c)| {
            if i % 2 == 0 {
                positions.push(i);
            }
            File {
                id: if i % 2 == 0 { Some(i / 2) } else { None },
                size: c.to_digit(10).expect("Input should parse.") as usize,
            }
        })
        .collect();

    if rep.last().unwrap().id.is_none() {
        rep.pop();
    }

    let highest = positions.len() - 1;

    for i in (1..=highest).rev() {
        let file_index = positions[i];

        if let Some(gap_index) = rep
            .iter()
            .position(|f| f.id.is_none() && f.size >= rep[file_index].size)
            .filter(|&gap_index| gap_index < file_index)
        {
            rep[file_index - 1].size += rep[file_index].size;
            rep[gap_index].size -= rep[file_index].size;

            rep.insert(gap_index, rep[file_index]);
            rep.remove(file_index + 1);

            positions[i] = gap_index;
            positions
                .iter_mut()
                .filter(|&&mut t| gap_index < t && t < file_index)
                .for_each(|t| *t += 1);
        }
    }

    rep.into_iter()
        .flat_map(|f| vec![f.id; f.size])
        .enumerate()
        .filter_map(|(i, x)| x.map(|t| t * i))
        .sum()
}

#[derive(Copy, Clone)]
struct File {
    id: Option<usize>,
    size: usize,
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day09.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 1928);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 6398608069280);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 2858);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 6427437134372);
    }
}
