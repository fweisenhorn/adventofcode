use crate::utils::{direction::Direction, position::Position, HashSet};
use grid::Grid;
use rayon::prelude::*;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day06.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let (grid, mut guard_pos) = parse_input(input);

    let mut visited = vec![guard_pos];
    let mut dir = Direction::Up;

    while let Some(new_pos) = guard_pos.try_step_in_grid(dir, &grid) {
        if grid[new_pos] {
            dir.turn_assign_right();
            continue;
        }

        guard_pos = new_pos;
        visited.push(guard_pos);
    }

    visited.sort_unstable();
    visited.dedup();
    visited.len()
}

fn part_b(input: &str) -> usize {
    let (grid, mut guard_pos) = parse_input(input);

    let mut visited = HashSet::default();
    let mut dir = Direction::Up;

    let mut rayon_queue = vec![];

    while let Some(new_pos) = guard_pos.try_step_in_grid(dir, &grid) {
        if grid[new_pos] {
            dir.turn_assign_right();
            continue;
        }

        if visited.insert(new_pos) {
            rayon_queue.push((guard_pos, new_pos, dir));
        }

        guard_pos = new_pos;
    }

    rayon_queue
        .into_par_iter()
        .filter(|&(guard_pos, new_pos, dir)| {
            let mut tmp_grid = grid.clone();
            tmp_grid[new_pos] = true;
            let mut tmp_guard_pos = guard_pos;
            let mut tmp_dir = dir.turn_right();

            let mut tmp_visited = HashSet::default();
            tmp_visited.insert((tmp_guard_pos, tmp_dir));

            while let Some(tmp_new_pos) = tmp_guard_pos.try_step_in_grid(tmp_dir, &tmp_grid) {
                if tmp_grid[tmp_new_pos] {
                    tmp_dir.turn_assign_right();
                    continue;
                }

                tmp_guard_pos = tmp_new_pos;

                if !tmp_visited.insert((tmp_guard_pos, tmp_dir)) {
                    return true;
                }
            }

            false
        })
        .count()
}

fn parse_input(input: &str) -> (Grid<bool>, Pos) {
    let mut y = None;
    let x: Vec<Vec<bool>> = input
        .lines()
        .enumerate()
        .map(|(i, line)| {
            line.chars()
                .enumerate()
                .map(|(j, c)| match c {
                    '#' => true,
                    '.' => false,
                    '^' => {
                        y = Some(Pos::new(i, j));
                        false
                    }
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect();

    (Grid::from(x), y.unwrap())
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day06.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 41);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 5086);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 6);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 1770);
    }
}
