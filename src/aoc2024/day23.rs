use crate::utils::{HashMap, HashSet};
use std::collections::VecDeque;

const INPUT: &str = include_str!("inputs/day23.txt");

pub fn run() -> (usize, String) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let connections = parse_input(input);

    let mut out = 0;

    for (k1, v1) in &connections {
        for (n2, k2) in v1.iter().enumerate() {
            for k3 in v1.iter().skip(n2) {
                if connections[k2].contains(k3) && {
                    k1.starts_with('t') || k2.starts_with('t') || k3.starts_with('t')
                } {
                    out += 1;
                }
            }
        }
    }

    out / 3
}

fn part_b(input: &str) -> String {
    let connections = parse_input(input);
    let mut queue: VecDeque<Vec<&str>> = VecDeque::new();
    let mut cache: HashSet<Vec<&str>> = HashSet::default();

    for (k1, v1) in &connections {
        for (n2, k2) in v1.iter().enumerate() {
            for k3 in v1.iter().skip(n2) {
                if connections[k2].contains(k3) {
                    let mut t = vec![*k1, *k2, *k3];
                    t.sort_unstable();

                    add_to_queue_and_cache(t, &mut cache, &mut queue);
                }
            }
        }
    }

    while let Some(v) = queue.pop_front() {
        if queue.is_empty() {
            return v.join(",");
        }

        connections[v[0]]
            .iter()
            .filter(|t| !v.contains(t))
            .filter(|t| v.iter().all(|v2| connections[v2].contains(**t)))
            .for_each(|t| {
                let mut v2 = v.clone();

                v2.insert(v.binary_search(t).unwrap_err(), t);

                add_to_queue_and_cache(v2, &mut cache, &mut queue);
            });
    }

    unreachable!()
}

fn parse_input(input: &str) -> HashMap<&str, HashSet<&str>> {
    let mut out: HashMap<&str, HashSet<&str>> = HashMap::default();

    input.lines().for_each(|line| {
        let (a, b) = line.split_once('-').unwrap();

        out.entry(a).or_default().insert(b);
        out.entry(b).or_default().insert(a);
    });

    out
}

fn add_to_queue_and_cache<'a, 'b>(
    t: Vec<&'a str>,
    cache: &'b mut HashSet<Vec<&'a str>>,
    queue: &'b mut VecDeque<Vec<&'a str>>,
) where
    'a: 'b,
{
    if !cache.contains(&t) {
        cache.insert(t.clone());
        queue.push_back(t);
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day23.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 7);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 1411);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), "co,de,ka,ta");
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), "aq,bn,ch,dt,gu,ow,pk,qy,tv,us,yx,zg,zu");
    }
}
