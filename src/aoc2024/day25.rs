const INPUT: &str = include_str!("inputs/day25.txt");

pub fn run() -> (usize, String) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    let (locks, keys) = parse_input(input);

    locks
        .iter()
        .map(|lock| keys.iter().filter(|&key| lock & key == 0).count())
        .sum()
}

fn part_b(_: &str) -> String {
    '🎄'.to_string()
}

fn parse_input(input: &str) -> (Vec<u32>, Vec<u32>) {
    (
        input
            .split("\n\n")
            .filter(|s| s.starts_with('#'))
            .map(custom_parse)
            .collect(),
        input
            .split("\n\n")
            .filter(|s| s.starts_with('.'))
            .map(custom_parse)
            .collect(),
    )
}

fn custom_parse(schematic: &str) -> u32 {
    schematic.chars().fold(0, |acc, c| match c {
        '#' => (acc << 1) + 1,
        '.' => acc << 1,
        '\n' => acc,
        _ => unreachable!(),
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day25.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 3);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 2885);
    }
}
