use crate::utils::vec_freq_count;

const INPUT: &str = include_str!("inputs/day01.txt");

pub fn run() -> (u32, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u32 {
    let (mut lefts, mut rights) = parse_input(input);

    lefts.sort_unstable();
    rights.sort_unstable();

    lefts.iter().zip(rights).map(|(l, r)| l.abs_diff(r)).sum()
}

fn part_b(input: &str) -> usize {
    let (lefts, rights) = parse_input(input);

    let counts = vec_freq_count(&rights);

    lefts
        .iter()
        .filter_map(|l| counts.get(l).map(|r| *l as usize * r))
        .sum()
}

fn parse_input(input: &str) -> (Vec<u32>, Vec<u32>) {
    input
        .lines()
        .map(|line| {
            let (a, b) = line.split_once("   ").unwrap();
            (a.parse().unwrap(), b.parse().unwrap())
        })
        .fold((vec![], vec![]), |mut acc, (l, r)| {
            acc.0.push(l);
            acc.1.push(r);
            acc
        })
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day01.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 11);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 1580061);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b(TEST), 31);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 23046913);
    }
}
