use crate::utils::{HashMap, HashSet};
use std::ops::AddAssign;

const INPUT: &str = include_str!("inputs/day22.txt");

pub fn run() -> (u64, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> u64 {
    let mut secrets: Vec<u64> = input.lines().map(|line| line.parse().unwrap()).collect();

    for _ in 0..2000 {
        secrets = secrets.into_iter().map(generate_new_secret).collect();
    }

    secrets.iter().sum()
}

// TODO: Optimize
fn part_b(input: &str) -> usize {
    let mut out: HashMap<[i8; 4], usize> = HashMap::default();

    for mut m in input.lines().map(|line| line.parse().unwrap()) {
        let mut nums = Vec::with_capacity(2000);
        for _ in 0..=2000 {
            nums.push(m);
            m = generate_new_secret(m);
        }

        let diffs: Vec<i8> = nums
            .windows(2)
            .map(|x| (x[1] % 10) as i8 - (x[0] % 10) as i8)
            .collect();

        let mut seen = HashSet::default();

        for (n, x) in diffs.windows(4).enumerate() {
            if seen.insert(x) {
                out.entry([x[0], x[1], x[2], x[3]])
                    .or_default()
                    .add_assign((nums[n + 4] % 10) as usize);
            }
        }
    }

    out.into_values().max().unwrap()
}

const fn generate_new_secret(mut x: u64) -> u64 {
    x = ((x * 0x40) ^ x) % 0x100_0000;
    x = ((x / 0x20) ^ x) % 0x100_0000;

    ((x * 0x800) ^ x) % 0x100_0000
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day22.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 37327623);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 12759339434);
    }

    #[test]
    fn test_b() {
        assert_eq!(part_b("1\n2\n3\n2024"), 23);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 1405);
    }
}
