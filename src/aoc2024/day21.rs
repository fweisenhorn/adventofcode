use crate::utils::{
    direction::{Direction, NEIGHBOURS},
    position::Position,
};
use memoize::memoize;
use rustc_hash::FxHashMap;
use std::cmp::min;

type Pos = Position<usize>;

const INPUT: &str = include_str!("inputs/day21.txt");

pub fn run() -> (usize, usize) {
    (part_a(INPUT), part_b(INPUT))
}

fn part_a(input: &str) -> usize {
    both_parts(input, 3)
}

fn part_b(input: &str) -> usize {
    both_parts(input, 26)
}

fn both_parts(input: &str, n_robots: usize) -> usize {
    input
        .lines()
        .map(|line| {
            let n = line.strip_suffix('A').unwrap().parse::<usize>().unwrap();

            let mut v = vec![NUMERIC[0]];
            v.extend(line.chars().map(pos_of_char));

            n * v
                .windows(2)
                .map(|x| shortest_path(x[0], x[1], n_robots, 0))
                .sum::<usize>()
        })
        .sum()
}

#[memoize(CustomHasher: FxHashMap, HasherInit: FxHashMap::default())]
fn shortest_path(from: Pos, to: Pos, n_robots: usize, index: usize) -> usize {
    if index == n_robots {
        return 1;
    }

    let mut out = usize::MAX;

    let mut queue = vec![];
    queue.push((from, vec![DIRECTIONAL[0]]));

    while let Some((pos, path)) = queue.pop() {
        if pos == to {
            out = min(
                out,
                add_to_path(&path, DIRECTIONAL[0])
                    .windows(2)
                    .map(|x| shortest_path(x[0], x[1], n_robots, index + 1))
                    .sum::<usize>(),
            );
        } else {
            NEIGHBOURS
                .into_iter()
                .filter_map(|d| pos.try_steps(d, 1).zip(Some(d)))
                .filter(|(p, _)| is_valid_pos(p, index) && to.distance(p) < to.distance(&pos))
                .for_each(|(p, d)| {
                    queue.push((p, add_to_path(&path, pos_of_dir(d))));
                });
        }
    }

    out
}

fn add_to_path<T: Clone>(path: &[T], elem: T) -> Vec<T> {
    let mut v = path.to_owned();
    v.push(elem);
    v
}

const NUMERIC: [Pos; 11] = [
    Position::<usize>(3, 2),
    Position::<usize>(3, 1),
    Position::<usize>(2, 0),
    Position::<usize>(2, 1),
    Position::<usize>(2, 2),
    Position::<usize>(1, 0),
    Position::<usize>(1, 1),
    Position::<usize>(1, 2),
    Position::<usize>(0, 0),
    Position::<usize>(0, 1),
    Position::<usize>(0, 2),
];
const DIRECTIONAL: [Pos; 5] = [
    Position::<usize>(0, 2),
    Position::<usize>(0, 1),
    Position::<usize>(1, 1),
    Position::<usize>(1, 0),
    Position::<usize>(1, 2),
];

fn is_valid_pos(pos: &Pos, index: usize) -> bool {
    if index == 0 {
        NUMERIC.contains(pos)
    } else {
        DIRECTIONAL.contains(pos)
    }
}

fn pos_of_dir(d: Direction) -> Pos {
    DIRECTIONAL[NEIGHBOURS.into_iter().position(|dd| dd == d).unwrap() + 1]
}

fn pos_of_char(c: char) -> Pos {
    NUMERIC[['A', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        .into_iter()
        .position(|cc| cc == c)
        .unwrap()]
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod tests {
    use super::*;

    const TEST: &str = include_str!("tests/day21.txt");

    #[test]
    fn test_a() {
        assert_eq!(part_a(TEST), 126384);
    }

    #[test]
    fn result_a() {
        assert_eq!(part_a(INPUT), 163920);
    }

    #[test]
    fn result_b() {
        assert_eq!(part_b(INPUT), 204040805018350);
    }
}
